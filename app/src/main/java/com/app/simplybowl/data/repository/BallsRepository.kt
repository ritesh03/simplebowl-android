package com.app.simplybowl.data.repository

import com.app.simplybowl.data.db.AppDatabase
import com.app.simplybowl.data.db.entities.Balls
import com.app.simplybowl.data.db.entities.User
import retrofit2.Response

class BallsRepository(
    private val db: AppDatabase
) {
    fun saveBall(balls: Balls) = db.getBallsDao().upsert(balls)
    fun updateBall(balls: Balls) = db.getBallsDao().updateBall(balls)
    fun getBalls() = db.getBallsDao().getballs()
}