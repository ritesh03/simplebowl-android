package com.app.simplybowl.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.format.DateTimeFormatter


@Entity
data class Alleys(
    @PrimaryKey(autoGenerate = true)
    var alleyid: Int? = null,
    var name: String? = "",
    var alleyImage: String? = "",
    var address: String? = "",
    var numbersid: Int? = null,
    var laneSurface: String? = "",
    var numLanes: String? = "",
    var notes: String? = "",
    var createDate: String? = null,
    var updateDate: String? = null
) {

}


