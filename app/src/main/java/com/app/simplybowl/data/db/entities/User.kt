package com.app.simplybowl.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.format.DateTimeFormatter

const val CURRENT_USER_ID = 0

@Entity
data class User(
    var userid: Int? = null,
    var name: String? = "",
    var email: String? = "",
    var profileImage: String? = "",
    var trackLeague: Boolean? = true,
    var trackBowlingName: Boolean? = true,
    var trackBowlingBall: Boolean? = true,
    var trackOilpattern: Boolean? = true,
    var showBoards: Boolean? = true,
    var rightHand: Boolean? = true,
    var showKeypad: Boolean? = null,
    var showPinsdown: Boolean? = false,
    var backupData: Boolean? = false,
    var colorId: Int? = null,
    var defaultMode: String? = "",
    var createDate: String? = null,
    var updateDate: String? = null
) {
    @PrimaryKey(autoGenerate = false)
    var uid: Int = CURRENT_USER_ID
}


