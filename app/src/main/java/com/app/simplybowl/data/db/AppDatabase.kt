package com.app.simplybowl.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.app.simplybowl.data.db.daos.*
import com.app.simplybowl.data.db.entities.*
import com.app.simplybowl.utils.Constants.Companion.DataBaseName


@Database(
    entities = [User::class, Balls::class, Alleys::class, OilPattern::class, AlleyContacts::class],
    version = 6,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getUserDao(): UserDao
    abstract fun getBallsDao(): BallsDao
    abstract fun getAlleysDao(): AlleysDao
    abstract fun getOilPatternsDao(): OilPatternDao
    abstract fun getAlleyContactsDao(): AlleyContactsDao


    companion object {

        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        public fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                DataBaseName
            )
                .fallbackToDestructiveMigration()
                .build()
    }
}