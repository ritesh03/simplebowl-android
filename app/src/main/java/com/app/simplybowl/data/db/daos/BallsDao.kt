package com.app.simplybowl.data.db.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.app.simplybowl.data.db.entities.Alleys
import com.app.simplybowl.data.db.entities.Balls
import com.app.simplybowl.data.db.entities.CURRENT_USER_ID
import com.app.simplybowl.data.db.entities.User


@Dao
interface BallsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(balls: Balls): Long

    @Query("SELECT * FROM Balls")
    fun getballs(): LiveData<List<Balls>>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateBall(balls: Balls)
}