package com.app.simplybowl.data.db.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.app.simplybowl.data.db.entities.Alleys
import com.app.simplybowl.data.db.entities.Balls
import com.app.simplybowl.data.db.entities.CURRENT_USER_ID
import com.app.simplybowl.data.db.entities.User

@Dao
interface AlleysDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(alleys: Alleys): Long

    @Query("SELECT * FROM Alleys")
    fun getalleys(): LiveData<List<Alleys>>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAlley(alleys: Alleys)
}