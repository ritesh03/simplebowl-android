package com.app.simplybowl.data.models

import android.os.Parcelable
import com.app.simplybowl.data.db.entities.AlleyContacts
import kotlinx.android.parcel.Parcelize

@Parcelize
class AlleysModel : Parcelable{

    var alleyid: Int? = null
    var name: String? = ""
    var alleyImage: String? = ""
    var alleyContacts: List<AlleyContacts>? = null
    var address: String? = ""
    var numbersid: Int? = null
    var laneSurface: String? = ""
    var numLanes: String? = ""
    var notes: String? = ""
    var createDate: String? = null
    var updateDate: String? = null
}