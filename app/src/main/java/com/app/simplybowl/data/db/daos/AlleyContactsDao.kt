package com.app.simplybowl.data.db.daos

import androidx.room.*
import com.app.simplybowl.data.db.entities.AlleyContacts


@Dao
interface AlleyContactsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(numbers: AlleyContacts): Long

    @Query("SELECT * FROM AlleyContacts WHERE alleyid =:alleyId")
    open fun loadSingleAlley(alleyId: Int): List<AlleyContacts>?


    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAlleyContacts(alleyContacts: AlleyContacts)

    @Query("DELETE FROM AlleyContacts WHERE alleyid =:alleyId")
    open fun deleteAll(alleyId: Int?)
}