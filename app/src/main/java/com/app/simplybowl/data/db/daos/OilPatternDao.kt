package com.app.simplybowl.data.db.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.app.simplybowl.data.db.entities.Balls
import com.app.simplybowl.data.db.entities.CURRENT_USER_ID
import com.app.simplybowl.data.db.entities.OilPattern
import com.app.simplybowl.data.db.entities.User


@Dao
interface OilPatternDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(oilPattern: OilPattern): Long

    @Query("SELECT * FROM OilPattern")
    fun getoilpatterns(): LiveData<List<OilPattern>>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateOilPattern(oilPattern: OilPattern)
}