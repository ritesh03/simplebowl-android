package com.app.simplybowl.data.repository

import com.app.simplybowl.data.db.AppDatabase
import com.app.simplybowl.data.db.entities.Alleys
import com.app.simplybowl.data.db.entities.Balls
import com.app.simplybowl.data.db.entities.OilPattern
import com.app.simplybowl.data.db.entities.User
import retrofit2.Response

class OilPatternRepository(
    private val db: AppDatabase
) {
    fun saveOilPattern(oilPattern: OilPattern) = db.getOilPatternsDao().upsert(oilPattern)
    fun updateOilPattern(oilPattern: OilPattern) =
        db.getOilPatternsDao().updateOilPattern(oilPattern)

    fun getOilPatterns() = db.getOilPatternsDao().getoilpatterns()
}