package com.app.simplybowl.data.repository

import com.app.simplybowl.data.db.AppDatabase
import com.app.simplybowl.data.db.entities.User
import retrofit2.Response

class UserRepository(
    private val db: AppDatabase
) {
    fun saveUser(user: User) = db.getUserDao().upsert(user)
    fun getUser() = db.getUserDao().getuser()
}