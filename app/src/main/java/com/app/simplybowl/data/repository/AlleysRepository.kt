package com.app.simplybowl.data.repository

import android.os.AsyncTask
import com.app.simplybowl.data.db.AppDatabase
import com.app.simplybowl.data.db.entities.AlleyContacts
import com.app.simplybowl.data.db.entities.Alleys


class AlleysRepository(
    private val db: AppDatabase
) {
    fun saveAlley(alleys: Alleys) = db.getAlleysDao().upsert(alleys)
    fun updateAlley(alleys: Alleys) = db.getAlleysDao().updateAlley(alleys)
    fun saveAlleyContacts(alleyContacts: AlleyContacts) =
        db.getAlleyContactsDao().upsert(alleyContacts)

    fun updateAlleyContacts(alleyContacts: AlleyContacts) =
        db.getAlleyContactsDao().updateAlleyContacts(alleyContacts)

    fun getAlleys() = db.getAlleysDao().getalleys()


   

}