package com.app.simplybowl.data.db.entities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.material.internal.ManufacturerUtils
import kotlinx.android.parcel.Parcelize
import java.time.format.DateTimeFormatter

@Parcelize
@Entity
data class Balls(
    @PrimaryKey(autoGenerate = true)
    var ballid: Int? = null,
    var ballImage: String? = "",
    var name: String? = "",
    var sn: String? = "",
    var manufacturer: String? = "",
    var weight: String? = "",
    var strikePercent: Float? = 0.0f,
    var sparePercent: Float? = 0.0f,
    var construction: String? = "",
    var cover: String? = "",
    var core: String? = "",
    var flarePotential: String? = "",
    var surfaceOrig: String? = "",
    var surfaceCurr: String? = "",
    var laneConditions: String? = "",
    var drillsheetImage: String? = "",
    var purchaseDate: String? = "",
    var createDate: String? = "",
    var updateDate: String? = ""
) : Parcelable {

}


