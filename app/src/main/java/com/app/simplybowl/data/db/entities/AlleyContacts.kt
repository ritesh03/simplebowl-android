package com.app.simplybowl.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

var CURRENT_ALLEY_ID = 0

@Entity
data class AlleyContacts(

    @PrimaryKey(autoGenerate = true)
    var contactId: Int? = null,
    var phoneNumber: String? = "",
    var phoneType: String? = "",
    var contactName: String? = ""

) {

    var alleyId: Int? = CURRENT_ALLEY_ID

}