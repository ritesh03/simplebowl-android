package com.app.simplybowl.data.db.entities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.material.internal.ManufacturerUtils
import kotlinx.android.parcel.Parcelize
import java.time.format.DateTimeFormatter

@Parcelize
@Entity
data class OilPattern(
    @PrimaryKey(autoGenerate = true)
    var oilpatternId: Int? = null,
    var oilImage: String? = "",
    var name: String? = "",
    var categoryName: String? = "",
    var length: String? = "",
    var type: String? = "",
    var rule: String? = "",
    var notes: String? = "",
    var createDate: String? = null,
    var updateDate: String? = null
): Parcelable {

}


