package com.app.simplybowl

import android.app.Application
import com.app.simplybowl.data.db.AppDatabase
import com.app.simplybowl.data.repository.AlleysRepository
import com.app.simplybowl.data.repository.BallsRepository
import com.app.simplybowl.data.repository.OilPatternRepository
import com.app.simplybowl.data.repository.UserRepository
import com.app.simplybowl.ui.home.bowlingalleys.AddBowlingAlleyViewModelFactory
import com.app.simplybowl.ui.home.bowlingalleys.BowlingAlleyViewModelFactory
import com.app.simplybowl.ui.home.bowlingbowls.AddBowlingBallViewModelFactory
import com.app.simplybowl.ui.home.bowlingbowls.BowlingBowlsViewModelFactory
import com.app.simplybowl.ui.home.home.HomeViewModelFactory
import com.app.simplybowl.ui.home.oilpatterns.AddOilPatternViewModelFactory
import com.app.simplybowl.ui.home.oilpatterns.OilPatternViewModelFactory
import com.app.simplybowl.ui.home.profile.*

import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class SimplyBowlApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@SimplyBowlApplication))


        bind() from singleton { UserRepository( instance()) }
        bind() from singleton { BallsRepository( instance()) }
        bind() from singleton { AlleysRepository( instance()) }
        bind() from singleton { OilPatternRepository( instance()) }
        bind() from singleton { AppDatabase(instance()) }
        bind() from provider { ProfileViewModelFactory(instance()) }
        bind() from provider { GeneralViewModelFactory(instance()) }
        bind() from provider { AdvancedViewModelFactory(instance()) }
        bind() from provider { ScoringViewModelFactory(instance()) }
        bind() from provider { HomeViewModelFactory(instance()) }
        bind() from provider { BowlingBowlsViewModelFactory(instance() ) }
        bind() from provider { AddBowlingBallViewModelFactory(instance() ) }
        bind() from provider { AddBowlingAlleyViewModelFactory(instance() ) }
        bind() from provider { BowlingAlleyViewModelFactory(instance() ) }
        bind() from provider { OilPatternViewModelFactory(instance() ) }
        bind() from provider { AddOilPatternViewModelFactory(instance() ) }




    }

}