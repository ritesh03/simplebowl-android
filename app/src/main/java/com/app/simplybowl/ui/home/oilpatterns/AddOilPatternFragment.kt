package com.app.simplybowl.ui.home.oilpatterns

import android.app.Activity
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment

import androidx.databinding.DataBindingUtil

import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.OilPattern
import com.app.simplybowl.databinding.AddOilPatternFragmentBinding
import com.app.simplybowl.ui.home.DataEntryListener
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.ui.home.ImageSelectListener
import com.app.simplybowl.utils.*

import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class AddOilPatternFragment : Fragment(), KodeinAware, DataEntryListener, ImageSelectListener {
    /**
     *  Variables initialization
     */
    override val kodein by kodein()
    private val factory: AddOilPatternViewModelFactory by instance()

    companion object {
        fun newInstance() = AddOilPatternFragment()
    }

    var imagePath: String? = ""
    var mOilIDTemp: Int? = null
    private lateinit var viewModel: AddOilPatternViewModel
    lateinit var binding: AddOilPatternFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.add_oil_pattern_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(AddOilPatternViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this
viewModel.dataEntryListener=this
        setClicks()
        setViews()
        setEditViews()

        return binding?.root
    }

    private fun setEditViews() {
        if (arguments != null) {
            binding?.profile.setText(getString(R.string.edit_oil_pattern_title))
            //  var oilPattern = Gson().fromJson(requireArguments().getString("data"), OilPattern::class.java)
            var oilPattern: OilPattern? = requireArguments().getParcelable("data")

viewModel.oilPatternModel=oilPattern
            /*binding?.oilName.setText(oilPattern?.name)
            binding?.oilNotes.setText(oilPattern?.notes)
            binding?.oilRule.setText(oilPattern?.rule)
            binding?.oilType.setText(oilPattern?.type)
            binding?.oilLength.setText(oilPattern?.length)
            binding?.oilCategory.setText(oilPattern?.categoryName)*/
            imagePath = oilPattern?.oilImage
            mOilIDTemp = oilPattern?.oilpatternId
            binding?.addOilImage.loadImage(imagePath)
        } else {
            binding?.profile.setText(getString(R.string.add_oil_pattern_title))
        }
    }

    /**
     * This method contains all the initialization of views
     */
    private fun setViews() {
        binding?.titleLayout.setMarginTop()
    }


    /**
     * This method contains all the clicks linked with this layout
     */
    private fun setClicks() {
        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(HomeActivity.con)
                hideSoftKeyboard(context as Activity, binding?.parentLayout)
                return v?.onTouchEvent(event) ?: false
            }
        })
        binding?.cancel.setOnClickListener(View.OnClickListener {

            requireFragmentManager().popBackStackImmediate()
        })

        binding?.addOilImage.setOnClickListener(View.OnClickListener {
             binding?.addOilImage.selectImage(this,this)

        })
    }


    override fun onSuccess() {

        hideSoftKeyboard(context as Activity, binding?.parentLayout)
        requireFragmentManager().popBackStackImmediate()
    }

    override fun onFailure() {

    }

    override fun onSelctedImagePath(path: String?) {
        viewModel.oilPatternModel?.oilImage=path
    }


}
