package com.app.simplybowl.ui.home

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.app.simplybowl.R
import com.app.simplybowl.databinding.ActivityHomeBinding
import com.app.simplybowl.ui.intro.SplashActivity
import com.app.simplybowl.ui.home.home.HomeFragment
import com.app.simplybowl.ui.home.home.HomeNewFragment
import com.app.simplybowl.utils.windowUI

class HomeActivity : AppCompatActivity() {
    companion object {
        var con: Activity? = null

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var binding: ActivityHomeBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_home)
        con = this
        SplashActivity.introStatus="yes"

        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(con)
                return v?.onTouchEvent(event) ?: true
            }
        })

        val sharedPreferences: SharedPreferences =getSharedPreferences("simplyBowl",
            Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor =  sharedPreferences.edit()
        editor.putString("tutorialStatus","yes")
        editor.apply()
        editor.commit()

        val fragment: Fragment = HomeFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(
            R.id.parent_lay,
            fragment,
            ""
        )
        //  fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun onResume() {
        super.onResume()

        windowUI(con)


    }
}
