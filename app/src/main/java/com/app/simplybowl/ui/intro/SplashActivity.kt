package com.app.simplybowl.ui.intro

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.app.simplybowl.R
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.utils.windowUI

class SplashActivity : AppCompatActivity() {
    companion object {
        var con: Activity? = null
        var introStatus: String= ""
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        windowUI(this)
        Handler().postDelayed({
            // This method will be executed once the timer is over
            // Start your app main activity
            val sharedPreferences: SharedPreferences =getSharedPreferences("simplyBowl",
                Context.MODE_PRIVATE)
            /**
             * Code for the checking the status of tutorial screens are already viewd or not
             */
            if(sharedPreferences.getString("tutorialStatus","no") .equals("yes"))
            {
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                 finish()
            }
            else {

                val onboarding = Intent(this, OnboardingActivity::class.java)
                startActivity(onboarding)
                finish()

            }
        }, 3000)


    }
}
