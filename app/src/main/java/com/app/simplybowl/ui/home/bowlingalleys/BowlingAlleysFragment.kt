package com.app.simplybowl.ui.home.bowlingalleys

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle

import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import com.app.simplybowl.R
import com.app.simplybowl.data.db.AppDatabase
import com.app.simplybowl.data.db.entities.CURRENT_ALLEY_ID
import com.app.simplybowl.data.models.AlleysModel
import com.app.simplybowl.databinding.BowlingAlleysFragmentBinding
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.ui.home.HomeActivity.Companion.con
import com.app.simplybowl.utils.openFragment
import com.app.simplybowl.utils.openFragmentWithoutBundle
import com.app.simplybowl.utils.setMarginTop

import com.app.simplybowl.utils.windowUI
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class BowlingAlleysFragment : Fragment(), KodeinAware {

    /**
     *  Variables initialization
     */
    override val kodein by kodein()
    private val factory: BowlingAlleyViewModelFactory by instance()

    companion object {
        fun newInstance() = BowlingAlleysFragment()
    }

    private lateinit var viewModel: BowlingAlleysViewModel
    var tempAlleysList: ArrayList<AlleysModel>? = arrayListOf<AlleysModel>()
    lateinit var binding: BowlingAlleysFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.bowling_alleys_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(BowlingAlleysViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this

        setViews()
        setClicks()
        getDataFromDB()


        return binding?.root
    }

    /**
     * This method is used for getting the data from local db
     */
    private fun getDataFromDB() {
        viewModel.alleys.observe(viewLifecycleOwner, Observer {
            var alleys = it
            tempAlleysList!!.clear()
            //balls.get()
            if (alleys.size == 0) {
                binding?.noData.visibility = View.VISIBLE
            } else {
                binding?.noData.visibility = View.GONE
            }

            alleys.forEach()
            {
                var alleysModel = AlleysModel()
                alleysModel.alleyid = it.alleyid
                alleysModel.name = it.name
                alleysModel.alleyImage = it.alleyImage
                alleysModel.address = it.address
                alleysModel.laneSurface = it.laneSurface
                alleysModel.numLanes = it.numLanes
                alleysModel.notes = it.notes
                CURRENT_ALLEY_ID = it.alleyid!!
                val thread = Thread {
                    alleysModel.alleyContacts =
                        AppDatabase.buildDatabase(requireContext()).getAlleyContactsDao()
                            .loadSingleAlley(it.alleyid!!)

                }
                thread.start()
                tempAlleysList!!.add(alleysModel!!)
            }
            val adapter = BowlingAlleyAdapter(requireContext(), tempAlleysList!!)
            binding?.bowlingAlleyRecyclerView.setLayoutManager(LinearLayoutManager(getActivity()));
            binding?.bowlingAlleyRecyclerView.adapter = adapter
        })
    }


    /**
     * This method contains all the clicks linked with this layout
     */
    private fun setClicks() {
        binding?.bowlingAlleyRecyclerView.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                if (tempAlleysList!!.size == 1) {
                    val adapter = BowlingAlleyAdapter(context!!, tempAlleysList!!)
                    binding?.bowlingAlleyRecyclerView.setLayoutManager(
                        LinearLayoutManager(
                            getActivity()
                        )
                    );
                    binding?.bowlingAlleyRecyclerView.adapter = adapter

                }
              //  windowUI(con!!)
                return v?.onTouchEvent(event) ?: true
            }
        })

        binding?.addIcon.setOnClickListener(View.OnClickListener {
            openFragmentWithoutBundle(requireFragmentManager(), AddBowlingAlleyFragment())
        })
        binding?.backBtn.setOnClickListener(View.OnClickListener {

            requireFragmentManager().popBackStackImmediate()
        })

    }

    /**
     * This method contains all the initialization of views
     */
    private fun setViews() {
        binding?.titleLayout.setMarginTop()
        binding?.profile.setText(getString(R.string.bowling_alley_title))
        binding?.home.visibility = View.GONE
        binding?.shareIcon.visibility = View.GONE
        binding?.backBtn.visibility = View.VISIBLE
        binding?.addIcon.setImageDrawable(resources.getDrawable(R.drawable.add_icon))

        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(con!!)
                return v?.onTouchEvent(event) ?: false
            }
        })

    }


}
