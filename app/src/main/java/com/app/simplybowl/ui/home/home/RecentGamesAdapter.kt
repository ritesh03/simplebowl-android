package com.app.simplybowl.ui.home.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.simplybowl.R

class RecentGamesAdapter (val con: Context ) :
    RecyclerView.Adapter<RecentGamesAdapter.UserViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = UserViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_recent, parent, false)
    )
    override fun getItemCount() = 8
    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {

    }
    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {



    }
}