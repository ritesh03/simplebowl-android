package com.app.simplybowl.ui.home.bowlingalleys

import android.content.Context
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.simplybowl.R

import com.app.simplybowl.data.db.entities.User
import com.app.simplybowl.ui.home.bowlingalleys.AddBowlingAlleyFragment.Companion.listContactsTemp

import com.app.simplybowl.ui.home.home.profile.ProfileFragment
import kotlinx.android.synthetic.main.row.view.*


class NumbersAdapter(val con: Context?, var alleyId: String) :
    RecyclerView.Adapter<NumbersAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = UserViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.row, parent, false)
    )

    override fun getItemCount() = listContactsTemp?.size
    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        /**
         * Set dropdownd for type
         */
          val adapterrr: ArrayAdapter<String> = ArrayAdapter<String>(
            con!!,
            android.R.layout.simple_dropdown_item_1line, con.resources.getStringArray(R.array.type)
        )
        holder.spinner?.setAdapter(adapterrr)

        /**
         * Click for remove contact
         */
        holder.ivRemove.setOnClickListener(View.OnClickListener {

            listContactsTemp?.removeAt(position)
            notifyDataSetChanged()
        })


        /**
         * Set data on views
         */
        holder.etContactName.setText(listContactsTemp?.get(position).contactName)
        holder.alleyNumber.setText(listContactsTemp?.get(position).phoneNumber)
        holder.spinner.setText(listContactsTemp?.get(position).phoneType)


        /**
         *  Below written code is for saving data in local storage
         */
        holder.alleyNumber.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (position < listContactsTemp?.size) {
                    listContactsTemp?.get(position).phoneNumber =
                        holder.alleyNumber.rawText.toString()
                }

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {

            }
        })
        holder.etContactName.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (position < listContactsTemp?.size) {
                    listContactsTemp?.get(position).contactName =
                        holder.etContactName.text.toString()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {


            }
        })



        holder.spinner.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (position < listContactsTemp?.size) {
                    listContactsTemp?.get(position).phoneType = holder.spinner.text.toString()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {

            }
        })


        holder.spinner.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View,
                pos: Int,
                id: Long
            ) {

                listContactsTemp?.get(position).phoneType = holder.spinner.text.toString()
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {

            }
        })

    }

    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val spinner = view.spinnerType
        val ivRemove = view.ivRemove
        val etContactName = view.etContactName
        val alleyNumber = view.alleyNumber

    }
}