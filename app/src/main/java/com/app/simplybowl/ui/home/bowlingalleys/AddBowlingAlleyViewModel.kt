package com.app.simplybowl.ui.home.bowlingalleys

import android.os.AsyncTask
import androidx.lifecycle.ViewModel
import com.app.simplybowl.data.db.AppDatabase
import com.app.simplybowl.data.db.entities.AlleyContacts
import com.app.simplybowl.data.db.entities.Alleys
import com.app.simplybowl.data.db.entities.Balls

import com.app.simplybowl.data.repository.AlleysRepository
import com.app.simplybowl.data.repository.BallsRepository

class AddBowlingAlleyViewModel( private val repository: AlleysRepository) : ViewModel() {

    fun saveAlleyData(alleys: Alleys){
     //   repository.saveAlley(alleys)
        alleyAsynTask(repository,"add").execute(alleys)
    }

    fun updateAlleyData(alleys: Alleys){
        alleyAsynTask(repository,"update").execute(alleys)
    }
  fun saveNumbersData(alleyContacts: AlleyContacts){
      alleyContactsAsynTask(repository).execute(alleyContacts)
    }

    val alleys = repository.getAlleys()


    class alleyAsynTask(var repository: AlleysRepository,var type:String) : AsyncTask<Alleys, Void, Void>() {

        override fun doInBackground(vararg params: Alleys): Void? {
            if(type.equals("add"))
            {

                repository.saveAlley(params[0])
            }
            else{
                repository.updateAlley(params[0])
            }

            return null
        }
    }

    class alleyContactsAsynTask(var repository: AlleysRepository) : AsyncTask<AlleyContacts, Void, Void>() {

        override fun doInBackground(vararg params: AlleyContacts): Void? {
            repository.saveAlleyContacts(params[0])

            return null
        }
    }

}