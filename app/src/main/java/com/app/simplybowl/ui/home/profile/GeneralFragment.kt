package com.app.simplybowl.ui.home.profile

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.view.*
import android.widget.CompoundButton
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.User
import com.app.simplybowl.databinding.GeneralFragmentBinding
import com.app.simplybowl.ui.home.HomeActivity.Companion.con
import com.app.simplybowl.ui.home.home.profile.ProfileFragment
import com.app.simplybowl.ui.home.home.profile.ProfileFragment.Companion.mUserName
import com.app.simplybowl.ui.home.home.profile.ProfileFragment.Companion.mid_lay
import com.app.simplybowl.ui.home.home.profile.ProfileFragment.Companion.scroll_View
import com.app.simplybowl.ui.home.home.profile.ProfileFragment.Companion.userLocal
import com.app.simplybowl.utils.windowUI
import kotlinx.android.synthetic.main.profile_intro_first.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import outlander.showcaseview.ShowcaseViewBuilder


class GeneralFragment : Fragment(), KodeinAware {

    /**
     *  Variables initialization
     */
    companion object {
        fun newInstance() = GeneralFragment()
        lateinit var showCase1: LinearLayout
    }

    override val kodein by kodein()
    private lateinit var showcaseViewBuilder: ShowcaseViewBuilder
    lateinit var sharedPreferences: SharedPreferences
    private val factory: GeneralViewModelFactory by instance()
    private lateinit var viewModel: GeneralViewModel
    lateinit var binding: GeneralFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.general_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(GeneralViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this
        initViews()
        setData()
        saveData()
        setTutorials()



        return binding?.root
    }

    private fun initViews() {
        sharedPreferences = requireContext().getSharedPreferences(
            "simplyBowl",
            Context.MODE_PRIVATE
        )
        showCase1 = binding?.showCase1
        if (userLocal == null) {
            userLocal = User()
        }

        showcaseViewBuilder = ShowcaseViewBuilder.init(con)
        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(con)
                return v?.onTouchEvent(event) ?: true
            }
        })
    }

    private fun showcaseFirst() {


        showcaseViewBuilder?.setTargetView(showCase1)
            .setBackgroundOverlayColor(-0x74000000)
            .setBgOverlayShape(ShowcaseViewBuilder.FULL_SCREEN)
            .setRoundRectCornerDirection(ShowcaseViewBuilder.FULL_SCREEN)
            .setRoundRectOffset(
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    170f,
                    resources.displayMetrics
                )
            )
            .setShowcaseShape(ShowcaseViewBuilder.SHAPE_SKEW)
            .setHideOnTouchOutside(false)
            .setRingWidth(
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    6f,
                    resources.displayMetrics
                )
            )
            .setShowcaseMargin(
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    1f,
                    resources.displayMetrics
                )
            ) // .setMarkerDrawable(getResources().getDrawable(android.R.drawable.arrow_down_float), Gravity.BOTTOM)
            // .setDrawableLeftMargin(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()))
            .addCustomVieww(
                R.layout.profile_intro_a,
                Gravity.TOP,
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    0f,
                    resources.displayMetrics
                ),
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    45f,
                    resources.displayMetrics
                ),
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    0f,
                    resources.displayMetrics
                ),
                0f
            )

        // .addCustomView(R.layout.profile_intro_a)

        showcaseViewBuilder?.show()
        showcaseViewBuilder?.setClickListenerOnView(
            R.id.tvNext,
            View.OnClickListener {


                showcaseViewBuilder?.hide()
                showcaseSecond()

            })

    }

    private fun showcaseSecond() {


        showcaseViewBuilder?.setTargetView(mid_lay)
            .setBackgroundOverlayColor(-0x74000000)
            .setBgOverlayShape(ShowcaseViewBuilder.FULL_SCREEN)
            .setRoundRectCornerDirection(ShowcaseViewBuilder.FULL_SCREEN)
            .setRoundRectOffset(
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    170f,
                    resources.displayMetrics
                )
            )
            .setShowcaseShape(ShowcaseViewBuilder.SHAPE_SKEW)
            .setHideOnTouchOutside(false)
            .setRingWidth(
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    6f,
                    resources.displayMetrics
                )
            )
            .setShowcaseMargin(
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    1f,
                    resources.displayMetrics
                )
            ) // .setMarkerDrawable(getResources().getDrawable(android.R.drawable.arrow_down_float), Gravity.BOTTOM)
            // .setDrawableLeftMargin(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()))
            .addCustomVieww(
                R.layout.profile_intro_b,
                Gravity.BOTTOM,
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    0f,
                    resources.displayMetrics
                ),
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    -110f,
                    resources.displayMetrics
                ),
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    0f,
                    resources.displayMetrics
                ),
                0f
            )

        // .addCustomView(R.layout.profile_intro_a)

        showcaseViewBuilder?.show()
        showcaseViewBuilder?.setClickListenerOnView(
            R.id.tvNext,
            View.OnClickListener {
                showcaseViewBuilder?.hide()
                ProfileFragment.scroll_View?.scrollTo(0, scroll_View!!.getBottom())
                ProfileFragment.view_Pager?.currentItem = 1
                Handler().postDelayed({
                    showcaseThird()
                }, 200)


            })

    }

    private fun showcaseThird() {


        showcaseViewBuilder?.setTargetView(AdvancedFragment.intro3Lay)
            .setBackgroundOverlayColor(-0x74000000)
            .setBgOverlayShape(ShowcaseViewBuilder.FULL_SCREEN)
            .setRoundRectCornerDirection(ShowcaseViewBuilder.FULL_SCREEN)
            .setRoundRectOffset(
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    170f,
                    resources.displayMetrics
                )
            )
            .setShowcaseShape(ShowcaseViewBuilder.SHAPE_SKEW)
            .setHideOnTouchOutside(false)
            .setRingWidth(
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    6f,
                    resources.displayMetrics
                )
            )
            .setShowcaseMargin(
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    1f,
                    resources.displayMetrics
                )
            ) // .setMarkerDrawable(getResources().getDrawable(android.R.drawable.arrow_down_float), Gravity.BOTTOM)
            // .setDrawableLeftMargin(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()))
            .addCustomVieww(
                R.layout.profile_intro_c,
                Gravity.TOP,
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    0f,
                    resources.displayMetrics
                ),
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    45f,
                    resources.displayMetrics
                ),
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    0f,
                    resources.displayMetrics
                ),
                0f
            )

        // .addCustomView(R.layout.profile_intro_a)

        showcaseViewBuilder?.show()
        showcaseViewBuilder?.setClickListenerOnView(
            R.id.tvNext,
            View.OnClickListener {

                val editor: SharedPreferences.Editor =
                    sharedPreferences?.edit()
                editor.putString("introStatus", "no")
                editor.apply()
                editor.commit()

                ProfileFragment.view_Pager?.currentItem = 0
                ProfileFragment.scroll_View?.scrollTo(0, 0)

                showcaseViewBuilder?.hide()
            })

    }


    /**
     * Method for set values on view from Local DB
     */
    private fun setData() {

      /*  viewModel.user.observe(viewLifecycleOwner, Observer {
            userLocal = it
            if (it != null) {

                binding?.trackLeagueSwitch.isChecked = it.trackLeague!!
                binding?.trackBallNameSwitch.isChecked = it.trackBowlingName!!
                binding?.trackBowlingBallSwitch.isChecked = it.trackBowlingBall!!
                binding?.trackOilPatternSwitch.isChecked = it.trackOilpattern!!
            }
        })*/

    }

    /**
     * Method for Save data in Local DB
     */
    private fun saveData() {

        binding?.profileName.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {

                if (!binding?.profileName.text.isNullOrEmpty()) {

                    if (userLocal == null) {
                        userLocal = User()
                    }

                    ProfileFragment.userLocal?.name = binding?.profileName.text.toString()

                     viewModel.saveUserData(ProfileFragment.userLocal!!)

                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {

                mUserName?.text = s.toString()
            }
        })

        binding?.profileEmail.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (userLocal == null) {
                    userLocal = User()
                }
                if (!binding?.profileEmail.text.isNullOrEmpty()) {
                    ProfileFragment.userLocal?.email = binding?.profileEmail.text.toString()

                        viewModel.saveUserData(ProfileFragment.userLocal!!)

                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {

                //  mUserName!!.text=s.toString()
            }
        })

        binding?.trackLeagueSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position

            if (userLocal == null) {
                userLocal = User()
            }
            if (isChecked) {

                ProfileFragment.userLocal!!.trackLeague = true

                     viewModel.saveUserData(ProfileFragment.userLocal!!)

            } else {

                userLocal!!.trackLeague = false

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            }

        })

        binding?.trackBallNameSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position
            if (userLocal == null) {
                userLocal = User()
            }

            if (isChecked) {

                userLocal!!.trackBowlingName = true

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            } else {

                userLocal!!.trackBowlingName = false

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            }

        })
        binding?.trackBowlingBallSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position

            if (userLocal == null) {
                userLocal = User()
            }
            if (isChecked) {

                userLocal!!.trackBowlingBall = true

                     viewModel.saveUserData(ProfileFragment.userLocal!!)

            } else {

                userLocal!!.trackBowlingBall = false

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            }

        })
        binding?.trackOilPatternSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position

            if (userLocal == null) {
                userLocal = User()
            }
            if (isChecked) {

                ProfileFragment.userLocal!!.trackOilpattern = true

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            } else {

                userLocal!!.trackOilpattern = false

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            }

        })

    }

    /**
     * Methods for instructions in profile screen in sequence
     */
    private fun setTutorials() {
        if (sharedPreferences!!.getString("introStatus", "yes").equals("yes")) {

            val mDialogView = LayoutInflater.from(con).inflate(R.layout.profile_intro_first, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(con)
                .setView(mDialogView)

            // .setTitle("Login Form")
            //show dialog
            val mAlertDialog = mBuilder.show()
            //
            //  mAlertDialog.getWindow()!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            mAlertDialog.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent);

            mAlertDialog.setCanceledOnTouchOutside(false)
            //login button click of custom layout
            mDialogView.letsGoTv.setOnClickListener {
                //dismiss dialog

                showcaseFirst()

                mAlertDialog.dismiss()
                //get text from EditTexts of custom layout
            }
            mDialogView.maybe_later.setOnClickListener {
                //dismiss dialog
                mAlertDialog.dismiss()
                //get text from EditTexts of custom layout
            }

        }

    }


}
