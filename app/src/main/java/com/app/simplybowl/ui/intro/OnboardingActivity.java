package com.app.simplybowl.ui.intro;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.simplybowl.R;
import com.app.simplybowl.ui.home.HomeActivity;

import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator;

import static com.app.simplybowl.utils.ViewUtilsKt.windowUI;


public class OnboardingActivity extends FragmentActivity {

    /**
     * Define Variables
     */
    private ViewPager pager;
    private TextView skip;
    private TextView simpleScoreTextView, trackYourTextView;
    private TextView next;
    RelativeLayout topIntroLay;
    private ScrollingPagerIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

        /**
         * Method for Variables initialization
         */
        init();

        /**
         * All clicks in this method for this screen
         */
        clicks();


    }

    private void clicks() {
        topIntroLay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                windowUI(OnboardingActivity.this);
                return false;


            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishOnboarding();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pager.getCurrentItem() == 2) {
                    finishOnboarding();
                } else {
                    pager.setCurrentItem(pager.getCurrentItem() + 1, true);
                }
            }
        });
    }

    private void init() {
        pager = (ViewPager) findViewById(R.id.pager);
        skip = findViewById(R.id.skip);
        topIntroLay = findViewById(R.id.topIntroLay);
        simpleScoreTextView = findViewById(R.id.simple_scor);
        trackYourTextView = findViewById(R.id.track_your_);
        indicator = findViewById(R.id.indicator);
        next = findViewById(R.id.next);
        FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return new OnboardingSimpleScoreTrackingFragment();
                    case 1:
                        return new OnboardingGameSettingsFragment();
                    case 2:
                        return new OnboardingTrackWhatsImpFragment();
                    case 3:
                        return new OnboardingUncoverHiddenTrendsFragment();
                    default:
                        return null;
                }
            }

            @Override
            public int getCount() {
                return 4;
            }
        };

        pager.setAdapter(adapter);
        indicator.attachToPager(pager);
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    simpleScoreTextView.setText(getResources().getString(R.string.simple_scor));
                    trackYourTextView.setText(getResources().getString(R.string.track_your_));

                } else if (position == 1) {
                    simpleScoreTextView.setText(getResources().getString(R.string.customize_g));
                    trackYourTextView.setText(getResources().getString(R.string.spend_more_));

                } else if (position == 2) {
                    simpleScoreTextView.setText(getResources().getString(R.string.track_what_));
                    trackYourTextView.setText(getResources().getString(R.string.track_the_s));

                } else if (position == 3) {
                    simpleScoreTextView.setText(getResources().getString(R.string.uncover_hid));
                    trackYourTextView.setText(getResources().getString(R.string.improve_you));

                }


                if (position == 3) {
                    //skip.setVisibility(View.GONE);
                    skip.setText("Done");
                } else {
                    skip.setVisibility(View.VISIBLE);
                    skip.setText("Skip");
                }
            }

        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        windowUI(this);

    }

    /**
     * Method for handling the status of onBoarding screens
     */
    private void finishOnboarding() {
        SharedPreferences preferences =
                getSharedPreferences("my_preferences", MODE_PRIVATE);

        preferences.edit()
                .putBoolean("onboarding_complete", true).apply();

        Intent main = new Intent(this, HomeActivity.class);
        startActivity(main);

        finish();
    }
}
