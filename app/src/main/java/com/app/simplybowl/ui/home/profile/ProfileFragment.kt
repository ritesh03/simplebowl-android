package com.app.simplybowl.ui.home.home.profile

import android.app.Activity
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.User
import com.app.simplybowl.databinding.ProfileFragmentBinding
import com.app.simplybowl.ui.home.HomeActivity.Companion.con
import com.app.simplybowl.ui.home.bowlingalleys.BowlingAlleysFragment
import com.app.simplybowl.ui.home.bowlingbowls.BowlingBowlsFragment
import com.app.simplybowl.ui.home.home.HomeFragment
import com.app.simplybowl.ui.home.oilpatterns.AddOilPatternFragment
import com.app.simplybowl.ui.home.oilpatterns.OilPatternsFragment
import com.app.simplybowl.ui.home.profile.*
import com.app.simplybowl.utils.openFragment
import com.app.simplybowl.utils.openFragmentWithoutBundle
import com.app.simplybowl.utils.setMarginTop
import com.app.simplybowl.utils.windowUI
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.tabs.TabLayout
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.File

class ProfileFragment : Fragment(), KodeinAware {
    /**
     *  Variables initialization
     */
    override val kodein by kodein()
    private lateinit var viewModel: ProfileViewModel
    lateinit var binding: ProfileFragmentBinding
    private val factory: ProfileViewModelFactory by instance()

    companion object {
        var mid_lay: LinearLayout? = null
        var view_Pager: ViewPager? = null
        var mUserName: TextView? = null
        var scroll_View: NestedScrollView? = null
        var userLocal: User? = User()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.profile_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(ProfileViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this



        setViews()
        setClicks()
        getDataFromDB()
        return binding?.root
    }


    /**
     * Method for getting data from local DataBase
     */
    private fun getDataFromDB() {
        userLocal = User()
        viewModel.user.observe(viewLifecycleOwner, Observer {
            userLocal = it

            if (it != null && !it.profileImage.isNullOrEmpty()) {
                binding?.profilePic.setImageURI(Uri.fromFile(File(it.profileImage)))
            } else {
                binding?.profilePic.setImageResource(R.drawable.dummy_placeholder)
            }

        })

    }


    /**
     * This method contains all the clicks linked with this layout
     */
    private fun setClicks() {
        binding?.profilePic.setOnClickListener(View.OnClickListener {

            ImagePicker.with(this)
                .compress(1024) //Final image size will be less than 1 MB(Optional)
                //.crop()
                // .maxResultSize(1080, 1080)  //Final image resolution will be less than 1080 x 1080(Optional)
                .start { resultCode, data ->
                    if (resultCode == Activity.RESULT_OK) {
                        //Image Uri will not be null for RESULT_OK
                        val fileUri = data?.data
                        //  binding!!.profilePic.setImageURI(fileUri)


                        //You can also get File Path from intent
                        val filePath: String = ImagePicker.getFilePath(data).toString()
                        if (userLocal == null) {
                            userLocal = User()
                        }

                        if (filePath != null) {
                            ProfileFragment.userLocal!!.profileImage = filePath
                        }
                        binding?.profilePic.setImageURI(Uri.fromFile(File("/sdcard/sample.jpg")))

                            // performing some dummy time taking operation
                            viewModel.saveUserData(ProfileFragment.userLocal!!)

                    } else if (resultCode == ImagePicker.RESULT_ERROR) {
                        Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                    }
                }

        })
        binding?.home.setOnClickListener(View.OnClickListener {
            windowUI(con)
            requireFragmentManager().popBackStackImmediate()
        })

        binding?.bowlingBalLay.setOnClickListener(View.OnClickListener {


            openFragmentWithoutBundle(requireFragmentManager() , BowlingBowlsFragment())

        })


        binding?.oilPatternLay.setOnClickListener(View.OnClickListener {

            openFragmentWithoutBundle(requireFragmentManager() , OilPatternsFragment())

        })

        binding?.bowlingAlleyLay.setOnClickListener(View.OnClickListener {


            openFragmentWithoutBundle(requireFragmentManager() , BowlingAlleysFragment())

        })
    }

    /**
     * This method contains all the initialization of views
     */
    private fun setViews() {
        binding?.titleLayout.setMarginTop()
        mid_lay = binding?.midLay
        mUserName = binding?.userName
        scroll_View = binding?.scrollView
        binding?.profile.setText(getString(R.string.profile_title))
        binding?.home.setText(getString(R.string.home_title))
        binding?.backBtn.visibility = View.GONE
        binding?.settingsIcon.setImageDrawable(resources.getDrawable(R.drawable.settings_icon))
        val adapter = ViewPagerAdapter(getChildFragmentManager())
        adapter.addFragment(GeneralFragment(), getString(R.string.general_title))
        adapter.addFragment(AdvancedFragment(), getString(R.string.advanced_title))
        adapter.addFragment(ScoringFragment(), getString(R.string.scoring_title))
        binding?.viewPager.adapter = adapter
        binding?.tabs.setupWithViewPager(binding?.viewPager)
        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(con)
                return v?.onTouchEvent(event) ?: true
            }
        })

        binding?.viewPagerLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(con)
                return v?.onTouchEvent(event) ?: true
            }
        })
        view_Pager = binding?.viewPager
        for (i in 0 until binding?.tabs.getTabCount()) {
            val tab: TabLayout.Tab = binding?.tabs.getTabAt(i)!!
            val relativeLayout: RelativeLayout = LayoutInflater.from(context)
                .inflate(R.layout.tab_layout, binding?.tabs, false) as RelativeLayout
            val tabTextView = relativeLayout.findViewById(R.id.tab_title) as TextView
            tabTextView.setText(tab.getText())
            tab.setCustomView(relativeLayout)
            //tab.select()
        }

    }


    // Code for tab layout for general, advanced and scoring tabs
    class ViewPagerAdapter(supportFragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(supportFragmentManager) {

        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }
    }


}
