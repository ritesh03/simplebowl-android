package com.app.simplybowl.ui.home.bowlingalleys

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.AlleyContacts
import com.app.simplybowl.ui.home.HomeActivity
import kotlinx.android.synthetic.main.item_alley_contact.view.*


class ViewNumbersAdapter(val con: Context, var contactsList: List<AlleyContacts>?) :
    RecyclerView.Adapter<ViewNumbersAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = UserViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_alley_contact, parent, false)
    )

    override fun getItemCount() = contactsList!!.size

    /**
     * Method for accessing phone call permissions
     */
    private fun setupPermissions(number: String) {
        val permission = ContextCompat.checkSelfPermission(
            con,
            Manifest.permission.CALL_PHONE
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {

            makeRequest()
        } else {
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number))
            con.startActivity(intent)

        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            con as Activity,
            arrayOf(Manifest.permission.CALL_PHONE),
            1
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {

        /**
         * Set data on views
         */
        holder.tvNumber.setText(contactsList?.get(position)?.phoneNumber)
        holder.tvTypeName.setText(contactsList?.get(position)?.contactName)
        if (contactsList?.get(position)?.phoneType.equals("cell")) {
            holder.ivType.setImageResource(R.drawable.phn_icon)
        } else if (contactsList?.get(position)?.phoneType.equals("Land")) {
            holder.ivType.setImageResource(R.drawable.phn_icon)
        } else if (contactsList?.get(position)?.phoneType.equals("Fax")) {
            holder.ivType.setImageResource(R.drawable.fax_icon)
        }

        /**
         * method for calling on number
         */
        holder.tvNumber.setOnClickListener(View.OnClickListener {
            setupPermissions(holder.tvNumber.unMaskedText.toString())
        })



    }

    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivType = view.ivType
        val tvTypeName = view.tvTypeName
        val tvNumber = view.tvNumber

    }
}