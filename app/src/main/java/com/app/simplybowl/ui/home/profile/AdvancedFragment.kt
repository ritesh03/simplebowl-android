package com.app.simplybowl.ui.home.profile

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.User
import com.app.simplybowl.databinding.AdvancedFragmentBinding
import com.app.simplybowl.databinding.GeneralFragmentBinding
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.ui.home.home.profile.ProfileFragment
import com.app.simplybowl.utils.windowUI
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

import org.kodein.di.generic.instance

class AdvancedFragment : Fragment(), KodeinAware {

    /**
     *  Variables initialization
     */
    override val kodein by kodein()
    companion object {
        fun newInstance() = AdvancedFragment()
       lateinit var intro3Lay: LinearLayout
    }
    private val factory: AdvancedViewModelFactory by instance()
    lateinit var binding: AdvancedFragmentBinding
    private lateinit var viewModel: AdvancedViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.advanced_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(AdvancedViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this

        intro3Lay = binding?.intro3
        if (ProfileFragment.userLocal == null) {
            ProfileFragment.userLocal = User()
        }

        saveData()
        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(HomeActivity.con)
                return v?.onTouchEvent(event) ?: true
            }
        })
        return binding?.root
    }




    /**
     * Method for Save data in Local DB
     */
    private fun saveData() {

        binding?.showBoardSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position
            if (ProfileFragment.userLocal !== null) {
                ProfileFragment.userLocal != User()
            }

            if (isChecked) {

                ProfileFragment.userLocal!!.showBoards = true

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            } else {

                ProfileFragment.userLocal!!.showBoards = false

                   viewModel.saveUserData(ProfileFragment.userLocal!!)

            }

        })

        binding?.handedSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position
            if (ProfileFragment.userLocal == null) {
                ProfileFragment.userLocal != User()
            }

            if (isChecked) {

                ProfileFragment.userLocal!!.rightHand = true
                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            } else {

                ProfileFragment.userLocal!!.rightHand = false

                   viewModel.saveUserData(ProfileFragment.userLocal!!)

            }

        })


    }


}

