package com.app.simplybowl.ui.home.bowlingalleys

import androidx.lifecycle.ViewModel
import com.app.simplybowl.data.db.entities.Alleys
import com.app.simplybowl.data.repository.AlleysRepository

class BowlingAlleysViewModel( private val repository: AlleysRepository) : ViewModel() {

     val alleys = repository.getAlleys()

}