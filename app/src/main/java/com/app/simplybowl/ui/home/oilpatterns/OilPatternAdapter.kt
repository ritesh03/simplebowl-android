package com.app.simplybowl.ui.home.oilpatterns

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.OilPattern
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.ui.home.bowlingbowls.AddBowlingBallFragment
import com.app.simplybowl.utils.loadImage
import com.app.simplybowl.utils.openFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_oil.view.*
import java.io.File

class OilPatternAdapter(val con: Context, var users: List<OilPattern>) :
    RecyclerView.Adapter<OilPatternAdapter.UserViewHolder>() {

    var temp_pos: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = UserViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_oil, parent, false)
    )

    override fun getItemCount() = users.size
    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {


        /**
         * set data on views
         */
        holder.bind(users[position].name, users.get(position).oilImage)
        var oilPattern = users.get(position)
        holder.tvCategory.setText(oilPattern.name)
        holder.tvLength.setText(oilPattern.length + "'")
        holder.tvType.setText(oilPattern.type)
        holder.tvRule.setText(oilPattern.rule + "'")
        holder.tvNotes.setText(oilPattern.notes + "")
        holder.shortLay.setOnClickListener(View.OnClickListener {
            temp_pos = position
            notifyDataSetChanged()
        })

        /**
         *  Condition for view only one open view
         */
        if (temp_pos == position) {
            holder.longLay.visibility = View.VISIBLE
            holder.shortLay.visibility = View.GONE
        } else {
            holder.longLay.visibility = View.GONE
            holder.shortLay.visibility = View.VISIBLE
        }
        //-----------------------------

        /**
         * Click of edit icon and transfer selected data to next screen
         */
        holder.ivEditOilPattern.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putString("type", "edit")
            bundle.putParcelable("data", users.get(position))

            openFragment((con as HomeActivity).getSupportFragmentManager(),bundle, AddOilPatternFragment())

        })
    }

    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val imageView = view.imageView
        private val patternImage = view.patternImage
        private val userName = view.textView
        val ivEditOilPattern = view.ivEditOilPattern
        val tvCategory = view.tvCategory
        val tvLength = view.tvLength
        val tvType = view.tvType
        val tvRule = view.tvRule
        val tvNotes = view.tvNotes
        private val bowling_title_name = view.bowling_title_name
        val shortLay = view.short_lay
        val longLay = view.long_lay

        fun bind(country: String?, img: String?) {
            userName.text = country
            bowling_title_name.text = country
            patternImage.loadImage(img)
            imageView.setImageURI(Uri.fromFile(File(img)))
        }
    }
}