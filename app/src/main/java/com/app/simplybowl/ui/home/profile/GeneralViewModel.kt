package com.app.simplybowl.ui.home.profile

import androidx.lifecycle.ViewModel
import com.app.simplybowl.data.db.entities.User
import com.app.simplybowl.data.repository.UserRepository

class GeneralViewModel(
    private val repository: UserRepository
) : ViewModel() {
    // val user = repository.getUser()
    fun saveUserData(user: User){
        UserAsynTask(repository).execute(user)
    }

    val user = repository.getUser()
}
