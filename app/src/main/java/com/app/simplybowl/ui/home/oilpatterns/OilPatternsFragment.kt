package com.app.simplybowl.ui.home.oilpatterns

import android.graphics.Color
import android.os.Build
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.OilPattern
import com.app.simplybowl.databinding.OilPatternsFragmentBinding
import com.app.simplybowl.ui.home.HomeActivity.Companion.con
import com.app.simplybowl.ui.home.bowlingbowls.AddBowlingBallFragment
import com.app.simplybowl.ui.home.bowlingbowls.BowlingBowlsAdapter
import com.app.simplybowl.utils.openFragment
import com.app.simplybowl.utils.openFragmentWithoutBundle
import com.app.simplybowl.utils.setMarginTop
import com.app.simplybowl.utils.windowUI
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class OilPatternsFragment : Fragment(), KodeinAware {
    /**
     *  Variables initialization
     */
    private lateinit var alleys: List<OilPattern>
    override val kodein by kodein()
    private val factory: OilPatternViewModelFactory by instance()

    companion object {
        fun newInstance() = OilPatternsFragment()
    }

    private lateinit var viewModel: OilPatternsViewModel
    lateinit var binding: OilPatternsFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.oil_patterns_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(OilPatternsViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this




        setViews()
        setClicks()
        getDataFromDb()



        return binding?.root
    }

    private fun getDataFromDb() {
        viewModel.oilPattern.observe(viewLifecycleOwner, Observer {
            alleys = it
            if (alleys?.size == 0) {
                binding?.noData.visibility = View.VISIBLE
            } else {
                binding?.noData.visibility = View.GONE
            }
            val adapter = OilPatternAdapter(requireContext(), alleys)
            binding?.bowlingRecyclerView.setLayoutManager(LinearLayoutManager(getActivity()));
            binding?.bowlingRecyclerView.adapter = adapter

        })

    }


    /**
     * This method contains all the clicks linked with this layout
     */
    private fun setClicks() {
        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(con!!)
                return v?.onTouchEvent(event) ?: false
            }
        })
        binding?.bowlingRecyclerView.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                if (alleys?.size == 1) {
                    val adapter = OilPatternAdapter(context!!, alleys)
                    binding?.bowlingRecyclerView.setLayoutManager(LinearLayoutManager(getActivity()));
                    binding?.bowlingRecyclerView.adapter = adapter


                }
                return v?.onTouchEvent(event) ?: true
            }
        })
        binding!!.addIcon.setOnClickListener(View.OnClickListener {



            openFragmentWithoutBundle(requireFragmentManager() , AddOilPatternFragment())

        })
        binding.backBtn.setOnClickListener(View.OnClickListener {


            requireFragmentManager().popBackStackImmediate()
        })
    }

    /**
     * This method contains all the initialization of views
     */
    private fun setViews() {
        binding?.titleLayout.setMarginTop()
        binding?.profile.setText(getString(R.string.oilPattern))
        binding?.home.visibility = View.GONE
        binding?.home.visibility = View.GONE
        binding?.shareIcon.visibility = View.GONE
        binding?.backBtn.visibility = View.VISIBLE
        binding?.addIcon.setImageDrawable(resources.getDrawable(R.drawable.add_icon))


    }


}
