package com.app.simplybowl.ui.home.bowlingalleys

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.simplybowl.data.repository.AlleysRepository
import com.app.simplybowl.data.repository.BallsRepository
import com.app.simplybowl.data.repository.UserRepository
import org.kodein.di.Kodein


@Suppress("UNCHECKED_CAST")
class AddBowlingAlleyViewModelFactory(
    private val repository: AlleysRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddBowlingAlleyViewModel(repository) as T
    }
}