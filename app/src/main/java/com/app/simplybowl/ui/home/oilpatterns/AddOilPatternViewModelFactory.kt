package com.app.simplybowl.ui.home.oilpatterns

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.simplybowl.data.repository.AlleysRepository
import com.app.simplybowl.data.repository.BallsRepository
import com.app.simplybowl.data.repository.OilPatternRepository
import com.app.simplybowl.data.repository.UserRepository
import org.kodein.di.Kodein


@Suppress("UNCHECKED_CAST")
class AddOilPatternViewModelFactory(
    private val repository: OilPatternRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddOilPatternViewModel(repository) as T
    }
}