package com.app.simplybowl.ui.home.oilpatterns

import androidx.lifecycle.ViewModel
import com.app.simplybowl.data.db.entities.Alleys
import com.app.simplybowl.data.db.entities.OilPattern
import com.app.simplybowl.data.repository.AlleysRepository
import com.app.simplybowl.data.repository.OilPatternRepository

class OilPatternsViewModel( private val repository: OilPatternRepository) : ViewModel() {

    val oilPattern = repository.getOilPatterns()

}