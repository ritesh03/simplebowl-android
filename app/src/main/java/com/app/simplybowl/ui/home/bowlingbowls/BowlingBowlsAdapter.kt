package com.app.simplybowl.ui.home.bowlingbowls

import android.app.AlertDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.Balls
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.ui.home.bowlingalleys.AddBowlingAlleyFragment
import com.app.simplybowl.utils.loadImage
import com.app.simplybowl.utils.openFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.image_dialog.view.*
import kotlinx.android.synthetic.main.view_header.view.*
import java.io.File

class BowlingBowlsAdapter(val con: Context, var users: List<Balls>?) :
    RecyclerView.Adapter<BowlingBowlsAdapter.UserViewHolder>() {

    var temp_pos: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = UserViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_header, parent, false)
    )

    override fun getItemCount() = users!!.size
    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {

        /**
         * Set data on views
         */
        var balls = users?.get(position)
        holder.tvSerial.setText(
            con.getString(R.string.serial_txt) +" "+ balls?.sn + "\n" + con.getString(R.string.purchase_date_label) +" "+ balls?.purchaseDate + "\n" + con.getString(
                            R.string.manufacturer_label) +
                    " "+balls?.manufacturer +
                    "\n" + con.getString(R.string.weight_label) +" "+ balls?.weight + con.getString(
                                    R.string.lbs) + "\n" + con.getString(R.string.strike_label)+" " + con.getString(
                                                    R.string.na)
        )
        holder.tvConstruction.setText(con.getString(R.string.construction_label)+" " + balls?.construction)
        holder.tvCover.setText(con.getString(R.string.cover_label)+" " + balls?.cover)
        holder.tvCore.setText(con.getString(R.string.core_label)+" " + balls?.core)
        holder.tvSurfaceOrig.setText(con.getString(R.string.surface_orig_label)+" " + balls?.surfaceOrig)
        holder.tvSurfaceCurr.setText(con.getString(R.string.surface_curr_label)+" " + balls?.surfaceCurr)
        holder.tvLaneCondition.setText(con.getString(R.string.lane_condition_label)+" " + balls?.laneConditions)
        holder.tvFlare.setText(con.getString(R.string.flare_potential_label)+" " + balls?.flarePotential)
        holder.bind(users?.get(position)?.name, users?.get(position)?.ballImage)


        /**
         * Code forn checking the current selected item
         */
        holder.shortLay.setOnClickListener(View.OnClickListener {
            temp_pos = position
            notifyDataSetChanged()
        })


        /**
         * Click of edit icon and transfer selected data to next screen
         */
        holder.ivEditBall.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putString("type", "edit")
            bundle.putParcelable("data", users?.get(position))

            openFragment((con as HomeActivity).getSupportFragmentManager(),bundle,AddBowlingBallFragment())

        })


        /**
         * Click for show drilling image
         */
        holder.showDrillingTv.setOnClickListener(View.OnClickListener {
            if (users?.get(position)?.drillsheetImage.isNullOrEmpty()) {
                Toast.makeText(con, con.getString(R.string.no_drilling_image), Toast.LENGTH_SHORT).show()
            } else {
                val mDialogView = LayoutInflater.from(con).inflate(R.layout.image_dialog, null)
                val mBuilder = AlertDialog.Builder(con)
                    .setView(mDialogView)
                //show dialog
                val mAlertDialog = mBuilder.show()
                mDialogView.viewDrillImage.loadImage(users?.get(position)?.drillsheetImage)
                //Close button click of custom layout
                mDialogView.closeIv.setOnClickListener {
                    //dismiss dialog
                    mAlertDialog.dismiss()
                }
            }
        })

        /**
         *  Condition for view only one open view
         */
        if (temp_pos == position) {
            holder.longLay.visibility = View.VISIBLE
            holder.shortLay.visibility = View.GONE

        } else {
            holder.longLay.visibility = View.GONE
            holder.shortLay.visibility = View.VISIBLE
        }
        //----------------------------------------
    }

    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val imageView = view.imageView
        val ivEditBall = view.ivEditBall
        private val ballImage = view.ball_image
        private val userName = view.textView
        private val tvBallName = view.tvBallname
        val tvSerial = view.tvSerial
        val tvCover = view.tvCover
        val tvConstruction = view.tvConstruction
        val tvCore = view.tvCore
        val tvSurfaceOrig = view.tvSurfaceOrig
        val tvSurfaceCurr = view.tvSurfaceCurr
        val tvFlare = view.tvFlare
        val tvLaneCondition = view.tvLaneCondition
        val showDrillingTv = view.showDrillingSheetTv
        val shortLay = view.short_lay
        val longLay = view.long_lay

        fun bind(name: String?, img: String?) {
            userName.text = name
            tvBallName.text = name
            imageView.loadImage(img)
            ballImage.loadImage(img)

        }
    }
}