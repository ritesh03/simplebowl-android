package com.app.simplybowl.ui.home.bowlingbowls

import androidx.lifecycle.ViewModel
import com.app.simplybowl.data.db.entities.Balls
import com.app.simplybowl.data.repository.BallsRepository

class BowlingBowlsViewModel( private val repository: BallsRepository) : ViewModel() {

    val balls = repository.getBalls()

}
