package com.app.simplybowl.ui.home.bowlingbowls

import android.os.AsyncTask

import android.view.View

import androidx.lifecycle.ViewModel

import com.app.simplybowl.data.db.entities.Balls

import com.app.simplybowl.data.repository.BallsRepository
import com.app.simplybowl.ui.home.DataEntryListener

class AddBowlingBallViewModel(private val repository: BallsRepository?) : ViewModel() {

    var ballsModel: Balls? = Balls()

    var dataEntryListener: DataEntryListener? = null

    val balls = repository?.getBalls()

    class ballsAsynTask(
        var dataEntryListener: DataEntryListener?,
        var repository: BallsRepository?
    ) : AsyncTask<Balls, Void, Void>() {

        override fun doInBackground(vararg params: Balls): Void? {

                repository?.saveBall(params[0])


            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            dataEntryListener?.onSuccess()
        }
    }

    fun onAddUpdateButtonClick(view: View) {

    ballsAsynTask(dataEntryListener, repository).execute(this.ballsModel)

    }
}