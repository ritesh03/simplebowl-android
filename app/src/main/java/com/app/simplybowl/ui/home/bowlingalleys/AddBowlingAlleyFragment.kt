package com.app.simplybowl.ui.home.bowlingalleys

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup

import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.simplybowl.BuildConfig
import com.app.simplybowl.R
import com.app.simplybowl.data.db.AppDatabase
import com.app.simplybowl.data.db.entities.AlleyContacts
import com.app.simplybowl.data.db.entities.Alleys
import com.app.simplybowl.data.models.AlleysModel
import com.app.simplybowl.databinding.AddBowlingAlleyFragmentBinding
import com.app.simplybowl.ui.home.ImageSelectListener
import com.app.simplybowl.utils.*
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode

import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.util.*


class AddBowlingAlleyFragment : Fragment(), KodeinAware, ImageSelectListener {
    /**
     *  Variables initialization
     */
    private lateinit var placesClient: PlacesClient
    var mAlleyID: Int? = null
    var mAlleyIDTemp: Int? = null
    var imagePath: String? = ""
    private val factory: AddBowlingAlleyViewModelFactory by instance()
    private lateinit var adapterr: NumbersAdapter
    lateinit var binding: AddBowlingAlleyFragmentBinding
    private lateinit var viewModel: AddBowlingAlleyViewModel

    override val kodein by kodein()

    companion object {
        fun newInstance() = AddBowlingAlleyFragment()
        lateinit var listContactsTemp: ArrayList<AlleyContacts>
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.add_bowling_alley_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(AddBowlingAlleyViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this

        setViews()
        setClicks()
        setEditView()

        /**
         * Code for checking the AlleyId
         */
        viewModel.alleys.observe(viewLifecycleOwner, Observer {
            var alleys = it
            if (alleys.size > 0) {
                mAlleyID = alleys.get(alleys.size - 1).alleyid!! + 1
            } else {
                mAlleyID = 1
            }
        })

        return binding?.root
    }

    private fun setEditView() {
        /**
         *  Set Data on screen when we come for edit
         */
        if (arguments != null) {
            binding?.profile.setText(getString(R.string.edit_bowling_alley_title))
            /*       var alleysModel =
                       Gson().fromJson(requireArguments().getString("data"), AlleysModel::class.java)
            */
            var alleysModel: AlleysModel? =
                requireArguments().getParcelable("data")
            binding?.alleyName.setText(alleysModel?.name)
            binding?.alleyNotes.setText(alleysModel?.notes)
            binding?.alleyLane.setText(alleysModel?.numLanes)
            binding?.spinner1.setText(alleysModel?.laneSurface)
            binding?.alleyAddress.setText(alleysModel?.address)
            imagePath = alleysModel?.alleyImage
            binding?.addBowlingAlleyImage.loadImage(imagePath)
            mAlleyIDTemp = alleysModel?.alleyid
            // listTemp=alleysModel.alleyContacts
            listContactsTemp = arrayListOf<AlleyContacts>()
            for (x in 0 until alleysModel?.alleyContacts!!.size) {
                listContactsTemp!!.add(alleysModel?.alleyContacts!!.get(x))
            }
            adapterr = NumbersAdapter(requireContext(), "")
            binding?.numbersRecyclerView.setLayoutManager(LinearLayoutManager(getActivity()));
            binding?.numbersRecyclerView.adapter = adapterr

        }
    }


    /**
     * This method contains all the clicks linked with this layout
     */
    private fun setClicks() {

        binding?.searchLay.setOnClickListener(View.OnClickListener {
            val placeFieldss: List<Place.Field> =
                ArrayList(Arrays.asList(*Place.Field.values()))
            val autocompleteIntentt = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY,
                placeFieldss
            )
                .build(requireActivity())
            startActivityForResult(autocompleteIntentt, 1001)
        })

        binding?.parentLayout.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                hideSoftKeyboard(context as Activity, binding?.parentLayout)
                windowUI(context as Activity)
                return v?.onTouchEvent(event) ?: true
            }
        })



        binding?.home.setOnClickListener(View.OnClickListener {

            requireFragmentManager().popBackStackImmediate()


        })

        binding?.tickIv?.setOnClickListener(View.OnClickListener {
            hideSoftKeyboard(context as Activity, binding?.parentLayout)
            if (arguments != null) {
                updateData()
            } else {
                saveData()
            }
            requireFragmentManager().popBackStackImmediate()


        })

        binding?.addBowlingAlleyImage.setOnClickListener(View.OnClickListener {
            binding?.addBowlingAlleyImage.selectImage(this,this)

        })

        binding?.addPhone?.setOnClickListener(View.OnClickListener {
            var numbers: AlleyContacts? = AlleyContacts()
            listContactsTemp!!.add(numbers!!)
            adapterr = NumbersAdapter(requireContext(), "")
            binding?.numbersRecyclerView.setLayoutManager(LinearLayoutManager(getActivity()));
            binding?.numbersRecyclerView.adapter = adapterr
        })
    }


    /**
     * Method for Save data in Local DB
     */
    private fun saveData() {
        var alleys = Alleys()
        alleys.name = binding?.alleyName.text.toString()
        alleys.notes = binding?.alleyNotes.text.toString()
        alleys.numLanes = binding?.alleyLane.text.toString()
        alleys.laneSurface = binding?.spinner1.text.toString()
        alleys.address = binding?.alleyAddress.text.toString()
        alleys.alleyImage = imagePath
        // performing some dummy time taking operation
        viewModel.saveAlleyData(alleys)
        for (item in listContactsTemp!!) {
            item.alleyId = mAlleyID
            saveNumbersData(item)
        }


    }

    /**
     * Method for Update data in Local DB
     */
    private fun updateData() {
        var alleys = Alleys()
        alleys.name = binding?.alleyName.text.toString()
        alleys.notes = binding?.alleyNotes.text.toString()
        alleys.numLanes = binding?.alleyLane.text.toString()
        alleys.laneSurface = binding?.spinner1.text.toString()
        alleys.address = binding?.alleyAddress.text.toString()
        alleys.alleyImage = imagePath
        alleys.alleyid = mAlleyIDTemp
deleteContactAsynTask(alleys,mAlleyIDTemp,requireContext(),viewModel).execute()




    }

    class deleteContactAsynTask(
        var alleys: Alleys,
        var mAlleyIDTemp: Int?,
        var requireContext: Context,
       var viewModel: AddBowlingAlleyViewModel
    ) : AsyncTask<Alleys, Void, Void>() {

        override fun doInBackground(vararg params: Alleys): Void? {
            AppDatabase.buildDatabase(requireContext).getAlleyContactsDao()
                .deleteAll(mAlleyIDTemp)
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

            viewModel.updateAlleyData(alleys)
            for (item in listContactsTemp!!) {
                item.alleyId = mAlleyIDTemp

                viewModel.saveNumbersData(item)
            }
        }
    }

    /**
     * Method for Save Numbers in Local DB
     */
    private fun saveNumbersData(numbers: AlleyContacts) {


        viewModel.saveNumbersData(numbers)


    }


    /**
     * This method contains all the initialization of views
     */
    private fun setViews() {
        binding?.titleLayout.setMarginTop()

        // Initialize Places.
        Places.initialize(requireContext(), BuildConfig.googleApiKeySimplyBowl)
        placesClient = Places.createClient(requireActivity())

        if (arguments == null) {
            binding?.profile.setText(getString(R.string.add_bowling_alley_title))
            listContactsTemp = arrayListOf<AlleyContacts>()
            var alleyContacts: AlleyContacts? = AlleyContacts()

            alleyContacts!!.phoneType = ""


            adapterr = NumbersAdapter(context, "")
            binding?.numbersRecyclerView.setLayoutManager(LinearLayoutManager(getActivity()));
            binding?.numbersRecyclerView.adapter = adapterr
        }

        val adapterrr: ArrayAdapter<String> = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_dropdown_item_1line,
            resources.getStringArray(R.array.lane_surface_array)
        )

        binding?.spinner1.setAdapter(adapterrr)


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {

                1001 -> if (resultCode == Activity.RESULT_OK) {
                    val placee: com.google.android.libraries.places.api.model.Place =
                        Autocomplete.getPlaceFromIntent(data!!)

                    binding?.alleyAddress.setText(placee.address)

                } else if (resultCode == 2) {
                    // TODO: Handle the error.
                    val status: Status =
                        Autocomplete.getStatusFromIntent(data!!)
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // The user canceled the operation.
                }
            }
        }
    }

    override fun onSelctedImagePath(path: String?) {
        imagePath = path
    }


}
