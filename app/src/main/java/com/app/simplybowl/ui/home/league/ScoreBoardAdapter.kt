package com.app.simplybowl.ui.home.league

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.simplybowl.R
import com.app.simplybowl.ui.home.home.RecentGamesAdapter

class ScoreBoardAdapter (val con: Context) :
    RecyclerView.Adapter<ScoreBoardAdapter.UserViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = UserViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_scoreboard, parent, false)
    )
    override fun getItemCount() = 5
    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {

    }
    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {



    }
}