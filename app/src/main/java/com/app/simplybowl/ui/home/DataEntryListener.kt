package com.app.simplybowl.ui.home


interface DataEntryListener {

    fun onSuccess()
    fun onFailure()
}