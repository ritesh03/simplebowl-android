package com.app.simplybowl.ui.home.oilpatterns

import android.os.AsyncTask
import android.view.View
import androidx.lifecycle.ViewModel
import com.app.simplybowl.data.db.entities.Alleys
import com.app.simplybowl.data.db.entities.OilPattern
import com.app.simplybowl.data.repository.AlleysRepository
import com.app.simplybowl.data.repository.OilPatternRepository
import com.app.simplybowl.ui.home.DataEntryListener
import com.app.simplybowl.ui.home.bowlingalleys.AddBowlingAlleyViewModel

class AddOilPatternViewModel( private val repository: OilPatternRepository) : ViewModel() {

    var dataEntryListener: DataEntryListener? = null

    var oilPatternModel: OilPattern? = OilPattern()

    fun saveOilPatternData(oilPattern: OilPattern){
        oilpatternAsynTask(dataEntryListener,repository).execute(oilPattern)
    }

    fun updateOilPatternData(oilPattern: OilPattern){
        oilpatternAsynTask(dataEntryListener,repository).execute(oilPattern)
    }

    class oilpatternAsynTask(var dataEntryListener: DataEntryListener?, var repository: OilPatternRepository) : AsyncTask<OilPattern, Void, Void>() {

        override fun doInBackground(vararg params: OilPattern): Void? {


                repository.saveOilPattern(params[0])


            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            dataEntryListener?.onSuccess()
        }
    }
    fun onAddUpdateButtonClick(view: View) {
        var temp: Double = 0.0
        if (!oilPatternModel?.length.equals("")) {
            //   var tempLenght: Double= Double.pa(binding!!.oilLength.text.toString())
            temp = oilPatternModel?.length.toString().toDouble() - 31
        }
        if (temp <= 0) {
            oilPatternModel?.rule="0"
        } else {
            oilPatternModel?.rule=temp.toString()
        }
        oilpatternAsynTask(dataEntryListener, repository).execute(oilPatternModel)

    }

}