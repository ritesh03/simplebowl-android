package com.app.simplybowl.ui.home.home

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager

import com.app.simplybowl.R
import com.app.simplybowl.databinding.FragmentHomeNewBinding
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.utils.windowUI
import com.google.android.material.bottomsheet.BottomSheetBehavior
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

class HomeNewFragment : Fragment() {


    lateinit var binding: FragmentHomeNewBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_home_new, container, false)
        val adapter = RecentGamesAdapter(requireContext())
        binding?.recentRecyclerview.setLayoutManager(LinearLayoutManager(getActivity()));
        binding?.recentRecyclerview.setHasFixedSize(true)
        binding?.recentRecyclerview.adapter = adapter

        var sheetBehavior = BottomSheetBehavior.from(binding?.bottomSheetLayout);
        // callback for do something
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                binding?.ivBlur.setVisibility(View.VISIBLE);
                binding?.ivBlur.setAlpha(slideOffset * 50);
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                windowUI(HomeActivity.con!!)
            }
        })
        // Inflate the layout for this fragment
        return binding?.root
    }


}
