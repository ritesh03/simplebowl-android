package com.app.simplybowl.ui.home.league

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.airbnb.lottie.LottieAnimationView
import com.app.simplybowl.R
import com.app.simplybowl.databinding.TimeToBallFragmentBinding
import com.bumptech.glide.load.engine.Resource


class TimeToBallFragment : Fragment() {

    companion object {
        fun newInstance() = TimeToBallFragment()
    }

    var binding: TimeToBallFragmentBinding? = null
    private lateinit var viewModel: TimeToBallViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.time_to_ball_fragment, container, false)
        viewModel = ViewModelProviders.of(this).get(TimeToBallViewModel::class.java)
        binding!!.viewmodel = viewModel
        binding!!.lifecycleOwner = this
binding!!.animationView.setAnimation("ball_shot.json")
        binding!!.animationView.playAnimation()
        binding!!.animationView.loop(true)
        setViews()
        setClick()
        return binding!!.root
    }

    /**
     * This method contains all the initialization of views
     */
    private fun setViews() {

    }

    /**
     * This method contains all the clicks linked with this layout
     */
    private fun setClick() {
        binding!!.letsGoTv.setOnClickListener(View.OnClickListener {
            /* val intent = Intent(context, LeagueActivity::class.java)
             context!!.startActivity(intent)*/
            fragmentManager!!.popBackStackImmediate()
        })
    }


}
