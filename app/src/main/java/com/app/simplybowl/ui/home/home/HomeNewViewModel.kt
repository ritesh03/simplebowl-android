package com.app.simplybowl.ui.home.home

import androidx.lifecycle.ViewModel
import com.app.simplybowl.data.db.entities.User
import com.app.simplybowl.data.repository.UserRepository

class HomeNewViewModel(
    private val repository: UserRepository
) : ViewModel() {

    val user = repository.getUser()
}
