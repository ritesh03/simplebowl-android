package com.app.simplybowl.ui.home.bowlingalleys

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.simplybowl.R
import com.app.simplybowl.data.models.AlleysModel
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.utils.Constants
import com.app.simplybowl.utils.loadImage
import com.app.simplybowl.utils.openFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_alley.view.*
import java.io.File


class BowlingAlleyAdapter(
    val con: Context,
    var users: ArrayList<AlleysModel>
) :
    RecyclerView.Adapter<BowlingAlleyAdapter.UserViewHolder>() {

    var tempPos: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = UserViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_alley, parent, false)
    )

    override fun getItemCount() = users.size
    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        var alleys = users.get(position)

        /**
         * Click of edit icon and transfer selected data to next screen
         */
        holder.ivEditAlley!!.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putString(Constants.Type, "edit")
            bundle.putParcelable(Constants.Data, users.get(position))

            openFragment((con as HomeActivity).getSupportFragmentManager(),bundle,AddBowlingAlleyFragment())
        })

        /**
         *  click to open map
         */
        holder.showMap.setOnClickListener(View.OnClickListener {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q=" + alleys.address)
            )
            con.startActivity(intent)
        })

        /**
         * set data on views
         */
        holder.tvAlleyAddress.setText(alleys.address)
        holder.tvAlleyLaneSurface.setText(alleys.laneSurface)
        holder.tvAlleyLane.setText(alleys.numLanes)
        holder.tvAlleyNotes.setText(alleys.notes)
        holder.shortLay.setOnClickListener(View.OnClickListener {
            tempPos = position
            notifyDataSetChanged()
        })


        /**
         *  Condition for view only one open view
         */
        if (tempPos == position) {
            holder.longLay.visibility = View.VISIBLE
            holder.shortLay.visibility = View.GONE


        } else {
            holder.longLay.visibility = View.GONE
            holder.shortLay.visibility = View.VISIBLE
        }
        //-----------------------------------

        holder.bind(users[position].name!!, users[position].alleyImage!!)
        val adapter = ViewNumbersAdapter(con!!, users[position].alleyContacts)
        holder.contactsRecyclerview.layoutManager = GridLayoutManager(con, 2)
        holder.contactsRecyclerview.adapter = adapter
    }


    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val imageView = view.ivAlleyImage
        val ivEditAlley = view.ivEditAlley
        val contactsRecyclerview = view.contactsRecyclerview
        private val tvAlleyName = view.tvAlleyName
        private val ivAlleyImage = view.iv_alley_image
        private val userName = view.bowling_alley_name
        val tvAlleyAddress = view.tvAlleySerial
        val showMap = view.showMap
        val tvAlleyLaneSurface = view.tvAlleyLaneSurface
        val tvAlleyLane = view.tvAlleyLane
        val tvAlleyNotes = view.tvAlleyNotes
        val shortLay = view.short_lay
        val longLay = view.long_lay

        fun bind(name: String, img: String) {
            userName.text = name
            tvAlleyName.text = name
            ivAlleyImage.loadImage(img)
            imageView.setImageURI(Uri.fromFile(File(img)))
        }
    }


}