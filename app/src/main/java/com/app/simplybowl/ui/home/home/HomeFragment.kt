package com.app.simplybowl.ui.home.home

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.simplybowl.R
import com.app.simplybowl.databinding.HomeFragmentBinding
import com.app.simplybowl.ui.home.HomeActivity.Companion.con
import com.app.simplybowl.ui.home.bowlingalleys.BowlingAlleysFragment
import com.app.simplybowl.ui.home.bowlingbowls.BowlingBowlsFragment
import com.app.simplybowl.ui.home.home.profile.ProfileFragment
import com.app.simplybowl.ui.home.league.TimeToBallFragment
import com.app.simplybowl.ui.home.oilpatterns.OilPatternsFragment
import com.app.simplybowl.utils.openFragment
import com.app.simplybowl.utils.openFragmentWithoutBundle
import com.app.simplybowl.utils.windowUI
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.File


class HomeFragment : Fragment(), KodeinAware {

    /**
     *  Variables initialization
     */
    override val kodein by kodein()
    companion object {
        fun newInstance() = HomeFragment()
    }
    lateinit var binding: HomeFragmentBinding
    private lateinit var behavior: BottomSheetBehavior<LinearLayout>
    private lateinit var behaviorRecent: BottomSheetBehavior<LinearLayout>
    private lateinit var viewModel: HomeViewModel
    private val factory: HomeViewModelFactory by instance()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(HomeViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this

        setViews()
        setClick()
        bottomSheetLayoutStatus()
        getDataFromDB()
        return binding?.root
    }

    /**
     * Method for getting data from local DB
     */
    private fun getDataFromDB() {
        viewModel.user.observe(viewLifecycleOwner, Observer {
            ProfileFragment.userLocal = it
            if (it != null) {
                if (it != null && !it.profileImage.isNullOrEmpty()) {
                    binding?.profileHome.setImageURI(Uri.fromFile(File(it.profileImage)))
                } else {
                    binding?.profileHome.setImageResource(R.drawable.dummy_placeholder)
                }

            } else {
                binding?.profileHome.setImageResource(R.drawable.dummy_placeholder)
            }
        })
    }


    /**
     * This method contains all the initialization of views
     */
    private fun setViews() {

        behavior = BottomSheetBehavior.from(binding?.bottomSheetLayout);
        behaviorRecent = BottomSheetBehavior.from(binding?.bottomSheet);
        val adapter = RecentGamesAdapter(requireContext())
        binding?.recentRecyclerview.setLayoutManager(LinearLayoutManager(getActivity()));
        binding?.recentRecyclerview.setHasFixedSize(true)
        binding?.recentRecyclerview.adapter = adapter
        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(con)
                return v?.onTouchEvent(event) ?: true
            }
        })
    }

    private fun bottomSheetLayoutStatus() {
        var sheetBehavior = BottomSheetBehavior.from(binding?.bottomSheetLayout);
        // callback for do something
        sheetBehavior.setBottomSheetCallback(object : BottomSheetCallback() {


            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                             binding?.ivBlur.setVisibility(View.VISIBLE);

                binding?.ivBlur.setAlpha(slideOffset * 50);

            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                windowUI(con)
            }
        })



        behaviorRecent?.setBottomSheetCallback(object : BottomSheetCallback() {


            override fun onSlide(bottomSheet: View, slideOffset: Float) {


            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                windowUI(con)
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {

                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }
        })
    }


    /**
     * This method contains all the clicks linked with this layout
     */
    private fun setClick() {

        binding?.imageViewArrow.setOnClickListener(View.OnClickListener {

            if (behavior?.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                behavior?.setState(BottomSheetBehavior.STATE_EXPANDED);

            } else {
                behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }

        })
        binding?.imageViewArrow1.setOnClickListener(View.OnClickListener {

            if (behaviorRecent?.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                behaviorRecent?.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);

            } else {
                behaviorRecent?.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }

        })

        binding?.bowlingBalLay.setOnClickListener(View.OnClickListener {

            behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);
            val bundle = Bundle()
            bundle.putString("type", "home")



            openFragment(requireFragmentManager(), bundle, BowlingBowlsFragment())
        })

        binding?.profileHome.setOnClickListener(View.OnClickListener {
            behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);
            openFragmentWithoutBundle(requireFragmentManager(), ProfileFragment())
        })


        binding?.oilPatternLay.setOnClickListener(View.OnClickListener {
            behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);

            val bundle = Bundle()
            bundle.putString("type", "home")



            openFragment(requireFragmentManager(), bundle, OilPatternsFragment())
        })

        binding?.bowlingAlleyLay.setOnClickListener(View.OnClickListener {
            behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);
            val bundle = Bundle()
            bundle.putString("type", "home")

            openFragment(requireFragmentManager(), bundle, BowlingAlleysFragment())
        })

        binding?.tvEditProfile.setOnClickListener(View.OnClickListener {
            behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);

            openFragmentWithoutBundle(requireFragmentManager() , ProfileFragment())
        })

        binding?.letSBowl.setOnClickListener(View.OnClickListener {
            windowUI(con)
            behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);

            openFragmentWithoutBundle(requireFragmentManager(),TimeToBallFragment())

        })

    }





}


