package com.app.simplybowl.ui.home.profile

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer

import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.User
import com.app.simplybowl.databinding.ScoringFragmentBinding
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.ui.home.home.profile.ProfileFragment
import com.app.simplybowl.utils.windowUI
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ScoringFragment : Fragment(), KodeinAware {

    /**
     *  Variables initialization
     */
    override val kodein by kodein()
    companion object {
        fun newInstance() = ScoringFragment()
    }
    private val factory: ScoringViewModelFactory by instance()
    private lateinit var viewModel: ScoringViewModel
    lateinit var binding: ScoringFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.scoring_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(ScoringViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this


        if (ProfileFragment.userLocal == null) {
            ProfileFragment.userLocal = User()
        }
        setData()
        saveData()
        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(HomeActivity.con)
                return v?.onTouchEvent(event) ?: true
            }
        })
        return binding?.root
    }

    /**
     * Method for set values on view from Local DB
     */
    private fun setData() {

        viewModel.user.observe(viewLifecycleOwner, Observer {
            ProfileFragment.userLocal = it
            if (it != null) {
                  if (it.defaultMode.equals("pins")) {
                    binding?.scoreingRadioGroup.check(R.id.default_pins)
                } else {
                    binding?.scoreingRadioGroup.check(R.id.default_scores)
                }
            }
        })

    }

    /**
     * Method for Save data in Local DB
     */
    private fun saveData() {
        binding?.scoreingRadioGroup.setOnCheckedChangeListener { group, checkedId ->

            if (ProfileFragment.userLocal == null) {
                ProfileFragment.userLocal = User()
            }
            if (R.id.default_pins == checkedId) {


                ProfileFragment.userLocal!!.defaultMode = "pins"

                    viewModel.saveUserData(ProfileFragment.userLocal!!)


            } else {
                ProfileFragment.userLocal!!.defaultMode = "scores"

                    viewModel.saveUserData(ProfileFragment.userLocal!!)


            }

        }

        binding?.pinDownSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position

            if (ProfileFragment.userLocal == null) {
                ProfileFragment.userLocal = User()
            }
            if (isChecked) {

                ProfileFragment.userLocal!!.showPinsdown = true

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            } else {

                ProfileFragment.userLocal!!.showPinsdown = false

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            }

        })

        binding?.backupSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position

            if (ProfileFragment.userLocal == null) {
                ProfileFragment.userLocal = User()
            }
            if (isChecked) {

                ProfileFragment.userLocal!!.backupData = true

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            } else {

                ProfileFragment.userLocal!!.backupData = false

                    viewModel.saveUserData(ProfileFragment.userLocal!!)

            }

        })


    }
}
