package com.app.simplybowl.ui.home.bowlingbowls

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.Balls
import com.app.simplybowl.databinding.BowlingBowlsFragmentBinding
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.ui.home.HomeActivity.Companion.con
import com.app.simplybowl.ui.home.bowlingalleys.BowlingAlleyAdapter
import com.app.simplybowl.ui.home.home.profile.ProfileFragment
import com.app.simplybowl.ui.home.profile.GeneralViewModelFactory
import com.app.simplybowl.utils.openFragment
import com.app.simplybowl.utils.openFragmentWithoutBundle
import com.app.simplybowl.utils.setMarginTop
import com.app.simplybowl.utils.windowUI

import kotlinx.android.synthetic.main.activity_home.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.File


class BowlingBowlsFragment : Fragment(), KodeinAware {
    /**
     *  Variables initialization
     */
    private lateinit var balls: List<Balls>
    override val kodein by kodein()
    companion object {
        fun newInstance() = BowlingBowlsFragment()
    }
    lateinit var binding: BowlingBowlsFragmentBinding
    private lateinit var viewModel: BowlingBowlsViewModel
    private val factory: BowlingBowlsViewModelFactory by instance()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.bowling_bowls_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(BowlingBowlsViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this


        setClicks()
        getDataFromDB()

        return binding?.root
    }

    /**
     * Method for getting data from local database
     */
    private fun getDataFromDB() {
        binding?.titleLayout.setMarginTop()
        viewModel.balls.observe(viewLifecycleOwner, Observer {
            balls = it
            if (balls?.size == 0) {
                binding?.noData.visibility = View.VISIBLE
            } else {
                binding?.noData.visibility = View.GONE
            }
            val adapter = BowlingBowlsAdapter(requireContext(), balls)
            binding?.bowlingRecyclerView.setLayoutManager(LinearLayoutManager(getActivity()));
            binding?.bowlingRecyclerView.adapter = adapter

        })
    }



    /**
     * This method contains all the clicks linked with this layout
     */
    private fun setClicks() {
        binding?.bowlingRecyclerView.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                if (balls?.size == 1) {
                    val adapter = BowlingBowlsAdapter(context!!, balls)
                    binding?.bowlingRecyclerView.setLayoutManager(LinearLayoutManager(getActivity()));
                    binding?.bowlingRecyclerView.adapter = adapter


                }
                return v?.onTouchEvent(event) ?: true
            }
        })
        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(con!!)
                return v?.onTouchEvent(event) ?: false
            }
        })
        binding?.addIcon.setOnClickListener(View.OnClickListener {


            openFragmentWithoutBundle(requireFragmentManager(),AddBowlingBallFragment())

        })
        binding?.backBtn.setOnClickListener(View.OnClickListener {

            requireFragmentManager().popBackStackImmediate()


        })
    }


}
