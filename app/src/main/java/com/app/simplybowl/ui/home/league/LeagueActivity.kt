package com.app.simplybowl.ui.home.league

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.simplybowl.R
import com.app.simplybowl.databinding.ActivityLeagueBinding
import com.app.simplybowl.ui.home.HomeActivity


class LeagueActivity : AppCompatActivity() {
    var binding: ActivityLeagueBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
          binding =
            DataBindingUtil.setContentView(this, R.layout.activity_league)

    setViews()

     setClick()
    }
    /**
     * This method contains all the initialization of views
     */
    private fun setViews() {
        val adapter = ScoreBoardAdapter(this!!)
        binding!!.scoreboardRecyclerview!!.setLayoutManager(LinearLayoutManager(this));
        binding!!.scoreboardRecyclerview!!.setHasFixedSize(true)
        binding!!.scoreboardRecyclerview!!.adapter = adapter
    }

    /**
     * This method contains all the clicks linked with this layout
     */
    private fun setClick() {
        binding!!.moreLayoutt!!.setOnClickListener(View.OnClickListener {
            binding!!.hideLayout!!.visibility = View.VISIBLE
            binding!!.moreLayoutt!!.visibility = View.GONE

        })
        binding!!.lessLayout!!.setOnClickListener(View.OnClickListener {
            binding!!.hideLayout!!.visibility = View.GONE
            binding!!.moreLayoutt!!.visibility = View.VISIBLE

        })

        binding!!.closeView!!.setOnClickListener(View.OnClickListener {
            binding!!.darkLay!!.visibility = View.GONE


        })

        binding!!.moreLay!!.setOnClickListener(View.OnClickListener {
            binding!!.darkLay!!.visibility = View.VISIBLE

        })
        binding!!.doneIcon!!.setOnClickListener(View.OnClickListener {
            binding!!.end!!.visibility = View.VISIBLE
            binding!!.endGamelay!!.visibility = View.VISIBLE
            binding!!.doneIcon!!.visibility = View.GONE
            binding!!.ballsLay!!.visibility = View.GONE
            binding!!.moreLay!!.visibility = View.GONE
            binding!!.tapTextView!!.visibility = View.GONE
            binding!!.strikeTextView!!.text = "New Game"

        })
        binding!!.end!!.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()

        })

        binding!!.strikeTextView!!.setOnClickListener(View.OnClickListener {
            if (binding!!.strikeTextView!!.text.equals("New Game")) {
                binding!!.strikeTextView!!.text = "Strike"
                binding!!.end!!.visibility = View.GONE
                binding!!.doneIcon!!.visibility = View.VISIBLE
            }

        })
    }
}
