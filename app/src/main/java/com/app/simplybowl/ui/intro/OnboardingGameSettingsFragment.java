package com.app.simplybowl.ui.intro;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.app.simplybowl.R;

import org.jetbrains.annotations.Nullable;

public class OnboardingGameSettingsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(
                R.layout.onboarding_screen2,
                container,
                false
        );

/**
 * Code for playing the lottie files
 */
        LottieAnimationView animation_view = view.findViewById(R.id.animation_view_tutorial);
        animation_view.setAnimationFromUrl("https://assets10.lottiefiles.com/packages/lf20_DUq3R8.json");

        animation_view.playAnimation();
        animation_view.loop(true);


        return view;


    }

}
