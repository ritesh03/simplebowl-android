package com.app.simplybowl.ui.home.bowlingbowls

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.simplybowl.data.repository.BallsRepository
import com.app.simplybowl.data.repository.UserRepository
import org.kodein.di.Kodein


@Suppress("UNCHECKED_CAST")
class AddBowlingBallViewModelFactory(
    private val repository: BallsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddBowlingBallViewModel(repository) as T
    }
}