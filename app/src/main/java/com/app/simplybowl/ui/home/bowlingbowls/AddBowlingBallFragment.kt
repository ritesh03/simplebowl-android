package com.app.simplybowl.ui.home.bowlingbowls

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.EditorInfo

import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.simplybowl.R
import com.app.simplybowl.data.db.entities.AlleyContacts
import com.app.simplybowl.data.db.entities.Balls
import com.app.simplybowl.data.models.AlleysModel
import com.app.simplybowl.databinding.AddBowlingBallFragmentBinding
import com.app.simplybowl.ui.home.DataEntryListener
import com.app.simplybowl.ui.home.HomeActivity
import com.app.simplybowl.ui.home.ImageSelectListener
import com.app.simplybowl.ui.home.bowlingalleys.AddBowlingAlleyFragment
import com.app.simplybowl.ui.home.bowlingalleys.NumbersAdapter
import com.app.simplybowl.utils.*
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.gson.Gson
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class AddBowlingBallFragment : Fragment(), KodeinAware, DataEntryListener, ImageSelectListener {
    /**
     *  Variables initialization
     */
    override val kodein by kodein()

    companion object {
        fun newInstance() = AddBowlingBallFragment()
    }

    var imagePath: String? = ""
    var drillImagePath: String? = ""
    private val factory: AddBowlingBallViewModelFactory by instance()
    private lateinit var viewModel: AddBowlingBallViewModel
    lateinit var binding: AddBowlingBallFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.add_bowling_ball_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(AddBowlingBallViewModel::class.java)
        binding?.viewmodel = viewModel
        binding?.lifecycleOwner = this
viewModel.dataEntryListener=this

        setViews()
        setClicks()
        setEditViews()


        return binding?.root
    }

    private fun setEditViews() {
        /**
         *  Set Data on screen when we come for edit
         */
        if (arguments != null) {
            binding?.profile.setText(getString(R.string.edit_bowling_ball_title))
          //  var balls = Gson().fromJson(requireArguments().getString("data"), Balls::class.java)

            var balls: Balls?=arguments?.getParcelable("data")
           viewModel.ballsModel=balls

            binding?.addBowlingImage.loadImage(imagePath)
            binding?.addDrillImage.loadImage(drillImagePath)
        } else {


            binding?.profile.setText(getString(R.string.add_bowling_ball_title))
        }

    }

    /**
     * This method contains all the initialization of views
     */
    private fun setViews() {
        binding?.titleLayout.setMarginTop()
        binding?.ballingManuf.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                binding?.ballingManuf.clearFocus()

                binding?.ballingPurchaseDate.performClick()
            }
            false
        })

        binding?.parentLayout.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                hideSoftKeyboard(context as Activity, binding?.parentLayout)
                return v?.onTouchEvent(event) ?: true
            }
        })
    }


    /**
     * This method contains all the clicks linked with this layout
     */
    private fun setClicks() {

        binding?.addBowlingImage.setOnClickListener(View.OnClickListener {

            binding?.addBowlingImage.selectImage(this,this)



        })

        binding?.addDrillImage.setOnClickListener(View.OnClickListener {
            ImagePicker.with(this)
                .compress(1024) //Final image size will be less than 1 MB(Optional)
                //.crop()
                // .maxResultSize(1080, 1080)  //Final image resolution will be less than 1080 x 1080(Optional)
                .start { resultCode, data ->
                    if (resultCode == Activity.RESULT_OK) {
                        //Image Uri will not be null for RESULT_OK
                        val fileUri = data?.data
                        //binding!!.addBowlingImage.setImageURI(fileUri)

                        //You can get File object from intent
                        val file: File = ImagePicker.getFile(data)!!

                        //You can also get File Path from intent
                        val filePath: String = ImagePicker.getFilePath(data).toString()

                        drillImagePath = filePath
                        viewModel.ballsModel?.drillsheetImage=drillImagePath
                        binding?.addDrillImage.loadImage(filePath)
                    } else if (resultCode == ImagePicker.RESULT_ERROR) {
                        Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                    }
                }

        })

        binding?.home.setOnClickListener(View.OnClickListener {

            requireFragmentManager().popBackStackImmediate()


        })
        binding?.parentLay.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                windowUI(HomeActivity.con!!)
                return v?.onTouchEvent(event) ?: false
            }
        })
        binding?.ballingPurchaseDate.setOnClickListener(View.OnClickListener {

            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(
                requireContext(),
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    //  Toast.makeText(this, """$dayOfMonth - ${monthOfYear + 1} - $year""", Toast.LENGTH_LONG).show()

                    val parser = SimpleDateFormat("dd - MM - yyyy")
                    val formatter = SimpleDateFormat("ddMMMyyyy")
                    val output: String =
                        formatter.format(parser.parse("""$dayOfMonth - ${monthOfYear + 1} - $year"""))
                    binding?.ballingPurchaseDate.setText(output)
                    binding?.ballingPurchaseDate.clearFocus()
                    binding?.ballingPurchaseDate.nextFocusForwardId = R.id.ballingWeight
                    binding?.ballingWeight!!.requestFocus()
                },
                year,
                month,
                day
            )
            dpd.show()
        })


    }

    override fun onSuccess() {
        hideSoftKeyboard(context as Activity, binding?.parentLayout)
        requireFragmentManager().popBackStackImmediate()

    }

    override fun onFailure() {

    }

    override fun onSelctedImagePath(path: String?) {
        viewModel.ballsModel?.ballImage=imagePath
    }

}
