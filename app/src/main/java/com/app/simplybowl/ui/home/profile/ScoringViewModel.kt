package com.app.simplybowl.ui.home.profile

import androidx.lifecycle.ViewModel
import com.app.simplybowl.data.db.entities.User
import com.app.simplybowl.data.repository.UserRepository

class ScoringViewModel(private val repository: UserRepository) : ViewModel() {
    fun saveUserData(user: User){
        UserAsynTask(repository).execute(user)
    }

    val user = repository.getUser()
}
