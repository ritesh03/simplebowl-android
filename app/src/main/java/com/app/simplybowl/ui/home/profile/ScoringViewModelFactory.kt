package com.app.simplybowl.ui.home.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.simplybowl.data.repository.UserRepository


@Suppress("UNCHECKED_CAST")
class ScoringViewModelFactory(
    private val repository: UserRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ScoringViewModel(repository) as T
    }
}