package com.app.simplybowl.utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.app.simplybowl.R
import com.app.simplybowl.ui.home.ImageSelectListener
import com.app.simplybowl.ui.home.oilpatterns.AddOilPatternFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.snackbar.Snackbar
import java.io.File


fun Context.toast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_LONG ).show()
}

fun ProgressBar.show(){
    visibility = View.VISIBLE
}

fun ProgressBar.hide(){
    visibility = View.GONE
}

fun View.snackbar(message: String){
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).also { snackbar ->
        snackbar.setAction("Ok") {
            snackbar.dismiss()
        }
    }.show()
}

fun ImageView.loadImage(uri: String?) {
    val options = RequestOptions()
       // .placeholder(R.drawable.loader)
      //  .circleCrop()
       // .error(R.mipmap.ic_launcher_round)
    Glide.with(this.context)
        .setDefaultRequestOptions(options)
        .load(uri)
        .into(this)
}
fun ImageView.selectImage(fragment: Fragment,imageSelectListener: ImageSelectListener) {

    ImagePicker.with(fragment)
        .compress(1024) //Final image size will be less than 1 MB(Optional)
        //.crop()
        // .maxResultSize(1080, 1080)  //Final image resolution will be less than 1080 x 1080(Optional)
        .start { resultCode, data ->
            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data


                //You can get File object from intent
                val file: File? = ImagePicker.getFile(data)

                //You can also get File Path from intent
                val filePath: String = ImagePicker.getFilePath(data).toString()
                //imagePath = filePath
                //viewModel.oilPatternModel?.oilImage=imagePath

                this.loadImage(filePath)
imageSelectListener.onSelctedImagePath(filePath)
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                this.context.toast(ImagePicker.getError(data))

            } else {

                this.context.toast("Task Cancelled")

            }
        }


}

fun windowUI(con:Activity?) {

 con?.window?.decorView?.setSystemUiVisibility(
        View.SYSTEM_UI_FLAG_LAYOUT_STABLE

                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

                 or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION // hide nav bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
//  or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                or View.SYSTEM_UI_FLAG_IMMERSIVE
    )
    con?.window?.apply {
        statusBarColor = Color.TRANSPARENT

    }

}

fun hideSoftKeyboard(con: Activity?, parentLayout: RelativeLayout) {

    val inputManager: InputMethodManager =
        con?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.hideSoftInputFromWindow(
        parentLayout.windowToken,
        InputMethodManager.SHOW_FORCED
    )

}


  fun openFragment(
      fragmentManager: FragmentManager,
      bundle: Bundle,
      fragment: Fragment
  ) {

    fragment.arguments = bundle
    val fragmentTransaction =
        fragmentManager.beginTransaction()
    fragmentTransaction.add(
        R.id.parent_lay,
        fragment,
        ""
    )
    fragmentTransaction.addToBackStack(null)
    fragmentTransaction.commitAllowingStateLoss()

}

  fun openFragmentWithoutBundle(
      fragmentManager: FragmentManager,
      fragment: Fragment
  ) {
    val fragmentTransaction =
        fragmentManager.beginTransaction()
    fragmentTransaction.add(
        R.id.parent_lay,
        fragment,
        ""
    )
    fragmentTransaction.addToBackStack(null)
    fragmentTransaction.commitAllowingStateLoss()
}


fun View.setMarginTop() {

    var statusBarHeight = 0
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        statusBarHeight = resources.getDimensionPixelSize(resourceId)
    }

    val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
    menuLayoutParams.setMargins(0, statusBarHeight, 0, 0)
    this.layoutParams = menuLayoutParams
}
