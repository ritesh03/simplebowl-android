package com.app.simplybowl.utils

class Constants {
    companion object {
        val DataBaseName = "SimplyBowl.db"
        val Type = "type"
        val Data = "data"
    }
}